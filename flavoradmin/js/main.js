$(document).ready(function() {
	$('.leftmenu').prepend('<div class="mobmenu"></div>');
	$('.mobmenu').hide();
    mobileMenu();

    $(window).resize(mobileMenu);

    $('.datepicker').datepicker();
});

function mobileMenu(){
    if ($(window).width() <= 992){	
		 $('.mobmenu').show();
		 $('.leftmenu>ul').slideUp();
		 $('.mobmenu').click(function(){
		   $('.leftmenu>ul').stop().slideToggle();
		 });
	}
	else{
		$('.mobmenu').hide();
		$('.leftmenu>ul').slideDown();
	}	
}
$(document).ready(function() {
	$('.diary_navigation > li > a').click(function(e) {
		e.preventDefault();
		var curBtn = $(this);
		$('.diary_navigation > li > a').not(curBtn).removeClass('active');
		curBtn.addClass('active');
		var targetDiaryBlock = $(curBtn.attr('href'));
		$('.diary_block').not(targetDiaryBlock).removeClass('active');
		targetDiaryBlock.addClass('active');
	});
	// Live search for product purchase
	$(document).on('keyup input', '#productCreateName', function (e) {
		e.preventDefault();
		var searchField = $(this),
			searchKeyWord = searchField.val(),
			form = searchField.closest('form'),
			liveList = searchField.siblings('#aj_live');
		if (searchKeyWord) {
			$.ajax({
				type: 'POST',
				url: '../services/product_purchase_live.php',
				data: {
					searchKey: searchKeyWord
				},
				success: function (result) {
					if (result.status == "ok") {
						liveList.empty();
						$.each(result.product_list, function (k, v) {
							liveList.append('<li data-pro_id="' + v.id + '" data-pro_name="' + v.product_name + '">' + v.product_name + '</li>');
						});
					} else {
						liveList.empty();
					}
				},
				error: function (error) {
					liveList.empty();
				}
			});
		} else {
			liveList.empty();
		}
	});
	$(document).on('click', '#aj_live > li', function () {
		var li = $(this),
			id = li.attr('data-pro_id'),
			name = li.attr('data-pro_name'),
			form = li.closest('form');
		form.find('#productCreateName').val(name);
		form.find('#productCreateId').val(id);
		li.parents('#aj_live').empty();
	});
});	