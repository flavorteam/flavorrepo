<?php include("../adminHeader.php") ?>

<?php
if($_SESSION['LogID']=="")
{
header("location:../../logout.php");
}
$db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE); 
$db->connect();
?>
<div class="row">
    <div class="col-lg-12">
        <div class="bd_panel bd_panel_default bd_panel_shadow">
            <form method="post" action="do.php?op=index" class="default_form" enctype="multipart/form-data">
                <div class="bd_panel_head">
                    <h3>Banner image upload</h3>
                </div>
                <div class="bd_panel_body">
                    <div class="row">
                        <div class="col-lg-4 col-md-4 col-sm-4 form_block_row">
                            <div class="form_block">
                                <label>Image</label>
                                <input type="file" name="image" id="image" >
                            </div>
                            
                        </div>
                    </div>
                </div>
                <div class="bd_panel_footer">
                    <div class="panel_row">
                        <div class="form_block_full">
                            <input type="submit" name="form" value="SAVE">
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
<?php
require('../admin_footer1.php');
require('../admin_footer2.php');
?>
