<?php
require("../../config/config.inc.php"); 
require("../../config/Database.class.php");
require("../../config/Application.class.php");

if($_SESSION['LogID']=="")
{
header("location:../../logout.php");
}
$loginId	=	$_SESSION['LogID'];
$loginType	=	$_SESSION['LogType'];
$optype=(strtolower(empty($_POST['op']))) ? ((strtolower(empty($_GET['op']))) ? $_REQUEST['op'] : $_GET['op']) : $_POST['op'];

switch($optype)
{
	// NEW SECTION
	case 'index':
		
		if(!$_REQUEST['productCreateId'])
			{
				$_SESSION['msg']="Error, Invalid Details!";			
				header("location:index.php");		
			}
			
		else
			{				
				$db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE); 
				$db->connect();
				$success1=0;							
				
				$name			=	$App->convert($_REQUEST['productCreateId']);
				$originalrate	=	$App->convert($_REQUEST['original_rate']);
				$flavorrate		=	$App->convert($_REQUEST['flavormart_rate']);
				//$cate			=	$App->convert($_REQUEST['categoryId']);
				$existId=$db->existValuesId(TABLE_PRODUCT_PURCHASE,"productCreateId='$name'");
				//echo $existId;die;
					if($existId>0)
					{
						$_SESSION['msg']="Product Is Already Exist";
					}
					else
					{		
						$data['productCreateId']			=	$name;
						$data['originalRate']	 			=	$App->convert($_REQUEST['original_rate']);
						$data['flavormartRate']	 			=	$App->convert($_REQUEST['flavormart_rate']);
				
						
						$data['homePage']					=	$App->convert($_REQUEST['homePage']);
						//$data['customerId']					= 0;
						
						$success1=$db->query_insert(TABLE_PRODUCT_PURCHASE,$data);//echo $success1;die;
						//echo $success1;die;
						//Category
						$catArray 	=	$_REQUEST['category'];
						//echo count($catArray);die;
						if(!empty($catArray)) 
						{
							for($j=0;$j<count($catArray);$j++)
							{
								$data1['purchaseId']			=	$success1;
							    $data1['categoryId']			=	$catArray[$j];
							   // echo $catArray[$i];die;
							    $db->query_insert(TABLE_PURCHASE_CATEGORIES,$data1);
							}
						    
						  // echo $_REQUEST['category'];die;
						}	
						
						$typeArray 	=	$_REQUEST['ptype'];
						//echo count($typeArray);die;
						if(!empty($typeArray)) 
						{
							for($i=0;$i<count($typeArray);$i++)
							{
								$data3['purchaseId']			=	$success1;
							    $data3['typeId']				=	$typeArray[$i];
							   //echo $typeArray[$i];die;
							    $db->query_insert(TABLE_PURCHASE_TYPE,$data3);
							}
						    
						  // echo $_REQUEST['category'];die;
						}	
						
						
						$img	=	"productPhoto/dummy.jpg";
						$data2['productId']			=	$success1;		
						$data2['picture']			=	$App->convert($img);								
																					
						$success2=$db->query_insert(TABLE_PRODUCT_PIC,$data2);	
							
						$db->close();
						
							if($success1)
							{							
								$_SESSION['msg']="Product Details Added Successfully";										
							}
							else{
								$_SESSION['msg']="Failed";
							}
					}					
				header("location:index.php");											
			}		
		break;		
	// EDIT SECTION
	case 'edit':		
		$editId	=	$_REQUEST['id']; 
			    	
		if(!$_REQUEST['productCreateId'])
			{
				$_SESSION['msg']="Error, Invalid Details!";					
				header("location:index.php");		
			}
		else
			{				
				$db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE); 
				$db->connect();
				$success1=0;
								
				$name		=	$App->convert($_REQUEST['productCreateId']);
				$name		=	ucwords(strtolower($name));
				
				$existId=$db->existValuesId(TABLE_PRODUCT_PURCHASE,"productCreateId='$name' and ID!='$editId'");
					if($existId>0)
					{
						$_SESSION['msg']="Product Is Already Exist";													
					}
					else
					{						
						$data['productCreateId']			=	$name;
						$data['productCreateId']			=	$name;
						$data['originalRate']	 			=	$App->convert($_REQUEST['original_rate']);
						$data['flavormartRate']	 		=	$App->convert($_REQUEST['flavormart_rate']);	
				
						$data['homePage']					=	$App->convert($_REQUEST['homePage']);
						
						$success1=$db->query_update(TABLE_PRODUCT_PURCHASE,$data," ID='{$editId}'");
						//delete query from table purchase categories
						$qry = "DELETE FROM ".TABLE_PURCHASE_CATEGORIES." where purchaseId=$editId";
						$resultQ = $db->query($qry);
						//echo $qry;
						$catArray 	=	$_REQUEST['category'];
						//echo count($catArray);die;
						if(!empty($catArray)) 
						{
							for($i=0;$i<count($catArray);$i++)
							{
								$data1['purchaseId']	=	$editId;
							    $data1['categoryId']	=	$catArray[$i];
							   // echo $catArray[$i];die;
							    $db->query_insert(TABLE_PURCHASE_CATEGORIES,$data1);
							}
						    
						  // echo $_REQUEST['category'];die;
						}	
						
						//delete query from table purchase type
						$qryType = "DELETE FROM ".TABLE_PURCHASE_TYPE." where purchaseId=$editId";
						$resultQ = $db->query($qryType);
						//echo $qry;
						$typeArray 	=	$_REQUEST['ptype'];
						if(!empty($typeArray)) 
						{
							for($j=0;$j<count($typeArray);$j++)
							{
								$data3['purchaseId']	=	$editId;
							    $data3['typeId']		=	$typeArray[$j];
							   // echo $catArray[$i];die;
							    $db->query_insert(TABLE_PURCHASE_TYPE,$data3);
							}
						    
						  // echo $_REQUEST['category'];die;
						}	
									
						$db->close();
						
							if($success1)
							{							
								$_SESSION['msg']="Product Details Updated Successfully";										
							}
							else{
								$_SESSION['msg']="Failed";
							}
					}					
				header("location:index.php");
														
			}	
			break;	
				
	// DELETE SECTION
	case 'delete':		
				$deleteId	=	$_REQUEST['id'];
				$success1	=	$success2	=	0;				
				
				$db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE); 
				$db->connect();				
								
				try
				{ 
					
					$success1	= @mysql_query("DELETE FROM `".TABLE_PRODUCT_PIC."` WHERE productId='{$deleteId}'");
					$success1	= @mysql_query("DELETE FROM `".TABLE_PRODUCT_PURCHASE."` WHERE ID='{$deleteId}'");	
				$success1	= @mysql_query("DELETE FROM `".TABLE_PURCHASE_CATEGORIES."` WHERE purchaseId='{$deleteId}'");			      
				}
				
				catch(Exception $e) 
				{
					 $_SESSION['msg']="You can't delete this.";				            
				}											
				$db->close(); 
				if($success1)
				{
					$_SESSION['msg']="Product details deleted successfully";										
				}	
				else
				{
					$_SESSION['msg']="You can't delete this.";										
				}	
				header("location:index.php");					
		break;
		
		
		// PHOTO UPLOAD
	case 'photo':				
			    	
		if(!$_REQUEST['productId'])
			{
				$_SESSION['msg']="Error, Invalid Details!";					
				header("location:index.php");		
			}
		else
			{			
				
				
				$db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE); 
				$db->connect();
				$success=0;
				
				$productId	=	$App->convert($_REQUEST['productId']);
				$existId	=	$db->existValuesId(TABLE_PRODUCT_PIC," picture='productPhoto/dummy.jpg' and productId='$productId'");		
				if($existId>0)
				{
					mysql_query("DELETE FROM `".TABLE_PRODUCT_PIC."` WHERE ID='{$existId}'");
				}
				// upload file	
				$date = date("YmdHis");
				$path_info =pathinfo($_FILES["picture"]["name"]);
				$ext	 	=	$path_info["extension"];	
				$allowed 	=  	array('gif','png','jpg');		// allowed image extensions					
				$newName 	= 	$date.".".$ext;
				
				if(in_array($ext,$allowed) ) 	
				{
					$img='';	
					if(move_uploaded_file($_FILES["picture"]["tmp_name"],"productPhoto/".basename($newName)))
					{
						$img="productPhoto/" . $newName;		
					}
					$data['productId']			=	$productId;		
					$data['picture']			=	$App->convert($img);								
																					
					$success1=$db->query_insert(TABLE_PRODUCT_PIC,$data);											
					$db->close();
				}
				if($success1)
				{							
					$_SESSION['msg']="Product Photo Added Successfully";										
				}
				else
				{
					if(!in_array($ext,$allowed) ) 
					{
						$_SESSION['msg']="You have uploaded an invalid image file";
					}
					else
					{
						$_SESSION['msg']="Failed";	
					}
				}
			}
			header("location:index.php");		
																			
			break;	
// DELETE  PHOTO
	case 'delPhoto':		
				$deleteId	=	$_REQUEST['deleteId'];
				$productId		=	$_REQUEST['sid'];
				$success1	=	0;				
				
				$db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE); 
				$db->connect();				
								
				try
				{ 
					
					$res=mysql_query("select picture,productId from ".TABLE_PRODUCT_PIC." where ID='{$deleteId}'");
					$row=mysql_fetch_array($res);
					$photo=$row['picture'];
					$product	=		$row['productId'];
					$success1= @mysql_query("DELETE FROM `".TABLE_PRODUCT_PIC."` WHERE ID='{$deleteId}'");
					if($success1)					
					{																																	
						if (file_exists($photo)) 	
						{
						unlink($photo);					
						} 						
					}
					
					$numPicQry	=	mysql_query("SELECT ID FROM ".TABLE_PRODUCT_PIC." WHERE productId=$product");
					$numPicRow	=	mysql_num_rows($numPicQry);
					if($numPicRow<1)
					{
						$img	=	"productPhoto/dummy.jpg";
						$data2['productId']			=	$product;		
						$data2['picture']			=	$App->convert($img);								
																					
						$success2=$db->query_insert(TABLE_PRODUCT_PIC,$data2);
					}
							
													     
				}
				catch(Exception $e) 
				{
					 $_SESSION['msg']="You can't delete. Because this data is used some where else";				            
				}											
				$db->close(); 
				if($success1)
				{
					$_SESSION['msg']="Product details deleted successfully";										
				}	
				else
				{
					$_SESSION['msg']="You can't delete. Because this data is used some where else";										
				}	
				header("location:index.php");				
		break;	
			
		
}


?>