<?php include("../adminHeader.php") ?>

<?php
if($_SESSION['LogID']=="")
{
header("location:../../logout.php");
}
$db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE); 
$db->connect();
?>
<script>
function delete_type()
{
var del=confirm("Do you Want to Delete ?");
	if(del==true)
	{
	window.submit();
	}
	else
	{
	return false;
	}
}

</script>
<script>
// ajax for loadin region
function loadMoreRegion(sid)
{

var xmlhttp;
if (window.XMLHttpRequest)
  {// code for IE7+, Firefox, Chrome, Opera, Safari
  xmlhttp=new XMLHttpRequest();
  }
else
  {// code for IE6, IE5
  xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
  }
xmlhttp.onreadystatechange=function()
  {
  if (xmlhttp.readyState==4 && xmlhttp.status==200)
    {
    document.getElementById("regionDiv"+sid).innerHTML=xmlhttp.responseText;
    }
  }
xmlhttp.open("GET","region.php?shopId="+sid,true);
xmlhttp.send();

}
</script>
<script>
	// ajax for loading photo

function loadMorePhoto(sid,pics)
{

var xmlhttp;
if (window.XMLHttpRequest)
  {// code for IE7+, Firefox, Chrome, Opera, Safari
  xmlhttp=new XMLHttpRequest();
  }
else
  {// code for IE6, IE5
  xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
  }
xmlhttp.onreadystatechange=function()
  {
  if (xmlhttp.readyState==4 && xmlhttp.status==200)
    {
    document.getElementById("photoDiv"+sid).innerHTML=xmlhttp.responseText;
    }
  }
xmlhttp.open("GET","photo.php?productId="+sid+"&pics="+pics,true);
xmlhttp.send();

}
</script>

<?php
 if(isset($_SESSION['msg'])){?><font color="red"><?php echo $_SESSION['msg']; ?></font><?php }	
 $_SESSION['msg']='';
 ?>
 
      <div class="col-md-10 col-sm-8 rightarea">
        <div class="row">
           <div class="col-sm-8"> 
          		<div class="clearfix">
					<h2 class="q-title">Product Purchase</h2> 
					<a href="#" class="addnew"  data-toggle="modal" data-target="#myModal"> <span class="plus">+</span> ADD New</a> 
				</div>
          </div>
          <div class="col-sm-4" >
            <form method="post">
              <div class="input-group">
                <input type="text" class="form-control" name="sname" placeholder="Product Name/Type" value="<?php echo @$_REQUEST['sname'] ?>">
                <span class="input-group-btn">
                <button class="btn btn-default lens" type="submit"></button>
                </span> </div>
            </form>
          </div>
        </div>
		 <?php	
		$cond="1";
		if(@$_REQUEST['sname'])
		{						
			$search		=	$_REQUEST['sname'];
			$cond=$cond." and ((".TABLE_PRODUCT_CREATION.".productName like'%".$search."%') or (".TABLE_PRODUCT_TYPE.".productType like'%".$search."%'))";
		}
		
		?>
		
        <div class="row">
          <div class="col-sm-12">
            <div class="tablearea table-responsive">
              <table class="table  view_limitter pagination_table" >
                <thead>
                  <tr>
						<th>SlNo</th>
						<th>Product</th>
						<th>Original Rate</th>
						<th>Flavormart Rate</th>
						<th>Product Type</th>
						<th>Categories</th>
						<th>Total Photos</th>
						<th>Photo</th>									
				 </tr>
                </thead>
                <tbody>
						<?php 		
																				
						$selAllQuery	=   "select ".TABLE_PRODUCT_PURCHASE.".ID,
													  ".TABLE_PRODUCT_CREATION.".productName,
													  ".TABLE_PRODUCT_PURCHASE.".originalRate,
													   ".TABLE_PRODUCT_PURCHASE.".flavormartRate,
													  ".TABLE_PRODUCT_PURCHASE.".homePage,
													
													(select (SELECT count(ID) as numPic FROM  ".TABLE_PRODUCT_PIC." where ".TABLE_PRODUCT_PIC.".productId=".TABLE_PRODUCT_PURCHASE.".ID)  AS count) as PhotoCount
													  
													 	
													from `".TABLE_PRODUCT_PURCHASE."`,`".TABLE_PRODUCT_CREATION."`
													   where `".TABLE_PRODUCT_PURCHASE."`.productCreateId =	`".TABLE_PRODUCT_CREATION."`.ID
													   and $cond
													     
													order by `".TABLE_PRODUCT_PURCHASE."`.ID desc";
						
						//echo $selAllQuery;
						$selectAll= $db->query($selAllQuery);
						$number		=	mysql_num_rows($selectAll);
						
						if($number==0)
						{
						?>
							<tr>
								<td align="center" colspan="9">
									There is no data in list.
								</td>
							</tr>
						<?php
						}
						else
						{
						/*********************** for pagination ******************************/
						$rowsPerPage = ROWS_PER_PAGE;
						if(isset($_GET['page']))
						{
							$pageNum = $_GET['page'];
						}
						else
						{
							$pageNum =1;
						}
						$offset = ($pageNum - 1) * $rowsPerPage;
						$select1=$db->query($selAllQuery." limit $offset, $rowsPerPage");
						$i=$offset+1;
						//use '$select1' for fetching
						/*************************** for pagination **************************/
						//$i=1;
						while($row=mysql_fetch_array($select1))
						{	
						$tableId	=	$row['ID'];
						
						//categoryQuery
						$qry = "SELECT ".TABLE_CATEGORIES.".categoryName
								  FROM ".TABLE_PURCHASE_CATEGORIES."
						     LEFT JOIN ".TABLE_CATEGORIES." ON 	".TABLE_CATEGORIES.".ID = ".TABLE_PURCHASE_CATEGORIES.".categoryId 
							     WHERE ".TABLE_PURCHASE_CATEGORIES.".purchaseId =$tableId";
								
						$catResult = $db->query($qry);
						$sCategory = array();
						while($catFetch = mysql_fetch_array($catResult))
						{
						 		//$sCategory = $catFetch['categoryName'];
						 		array_push($sCategory,$catFetch['categoryName']);
						}		
						//print_r($sCategory);
						$categoryNw	=	implode(",",$sCategory);
						
						//typeQuery
						
						$qryType = "SELECT ".TABLE_PRODUCT_TYPE.".productType
								  FROM ".TABLE_PURCHASE_TYPE."
						     LEFT JOIN ".TABLE_PRODUCT_TYPE." ON 	".TABLE_PRODUCT_TYPE.".ID = ".TABLE_PURCHASE_TYPE.".typeId 
							     WHERE ".TABLE_PURCHASE_TYPE.".purchaseId =$tableId";
						//echo $qryType;		
						$typeResult = $db->query($qryType);
						$sType = array();
						while($typeFetch = mysql_fetch_array($typeResult))
						{
						 		//$sCategory = $catFetch['categoryName'];
						 		array_push($sType,$typeFetch['productType']);
						}		
						//print_r($sCategory);
						$typeNw	=	implode(",",$sType);
						
						
						
						
						$numPics = $row['PhotoCount'];
						?>
                  <tr>
                    <td><?php echo $i;$i++;?>
                      <div class="adno-dtls"> <a href="edit.php?id=<?php echo $tableId?>">EDIT</a> | <a href="do.php?id=<?php echo $tableId; ?>&op=delete" class="delete" onclick="return delete_type();">DELETE</a>  | <a href="#" data-toggle="modal" data-target="#myModal3<?php echo $tableId; ?>"> VIEW</a> </div>
					    <!-- Modal3 -->
						  <div class="modal fade" id="myModal3<?php echo $tableId; ?>" tabindex="-1" role="dialog">
							<div class="modal-dialog">
							  <div class="modal-content"> 
								<!-- <div class="modal-body clearfix">
							fddhdhdhddhdh
							  </div>-->	 		
								<div role="tabpanel" class="tabarea2"> 
								  
								  <!-- Nav tabs -->
								  <ul class="nav nav-tabs" role="tablist">
									<li role="presentation" class="active"> <a href="#personal<?php echo $tableId; ?>" aria-controls="personal" role="tab" data-toggle="tab">Product Purchase Details</a> </li>	
																		
								  </ul>
								  
								  <!-- Tab panes --><!--for view-->
								  <div class="tab-content">
									<div role="tabpanel" class="tab-pane active" id="personal<?php echo $tableId; ?>">
									  <table class="table nobg" >
										<tbody style="background-color:#FFFFFF">
										
										
										  <tr>
										  	<td>Product Details</td>
										  	<td>:</td>
											<td><?php echo $row['productName']; ?></td>
										  </tr>
										  <tr>
											<td>Original Rate</td>
											<td>:</td>
											<td><?php echo $row['originalRate']; ?></td>
										  </tr>
										  <tr>
											<td>Flavormart Rate</td>
											<td>:</td>
											<td><?php echo $row['flavormartRate']; ?></td>
										  </tr>
										   <tr>
											<td>Product Type</td>
											<td>:</td>
											<td><?php echo $typeNw; ?></td>
										  </tr>
										  <tr>
											<td>Category</td>
											<td>:</td>
											<td><?php echo $categoryNw; ?></td>
										  </tr>
										  										  
										</tbody>
									  </table>
									</div>
								  </div>
								</div>

							  </div>
							</div>
						  </div>
						  <!-- Modal3 cls --> 					  
					  </td>                    					
					<td><?php echo $row['productName']; ?></td>				
					<td><?php echo $row['originalRate']; ?></td>
					<td><?php echo $row['flavormartRate']; ?></td>
					<td><?php echo $typeNw; ?></td>
					<td><?php echo $categoryNw; ?></td>
					<td><?php echo $numPics; ?></td>
					<!------------------------------------- Photo --------------------------------->															
					<td style="width: 108px;"><a href="#" data-toggle="modal" data-target="#myModal5<?php echo $tableId; ?>" class="viewbtn" onclick="loadMorePhoto('<?php echo $tableId;?>','<?php echo $numPics;?>')">Photo</a>						
              <!-- Modal3 photo -->
              <div class="modal fade" id="myModal5<?php echo $tableId; ?>" tabindex="-1" role="dialog">
                <div class="modal-dialog">
                  <div class="modal-content">
                    <div role="tabpanel" class="tabarea2">
                      <!-- Nav tabs -->
                      <ul class="nav nav-tabs" role="tablist">
                        <li role="presentation" class="active"> <a href="#photo<?php echo $tableId; ?>" aria-controls="personal" role="tab" data-toggle="tab">PRODUCT PHOTO</a> </li>
                      </ul>
                      <!-- Tab panes -->
                      <div class="tab-content" id="photoDiv<?php echo $tableId; ?>">
                       
                      </div>
                  </div>
                </div>
              </div>
              </div>
              <!-- Modal3 photo cls -->
            </td>    
            <!------------------------------------- Photo --------------------------------->   
						
               		</tr>
				  <?php }
			}
			?>                  
                </tbody>
              </table>			  			  
            </div>
             <!-- paging -->		
            <!--<div style="clear:both;"></div>
            <div class="text-center">
                <div class="btn-group pager_selector"></div>
            </div> -->       
            <!-- paging end-->
            
            <!--*****************************************************************-->
            
            	 <?php 
                  if($number>@$rowsPerPage)
					{
					?>	
					 <br />	
					  <div class="pagerSC" align="center">
					<?php
					
					$query   =  $db->query($selAllQuery);
					$numrows = mysql_num_rows($query);
					$maxPage = ceil($numrows/$rowsPerPage);
					$self = $_SERVER['PHP_SELF'];
					$nav  = '';
					if ($pageNum - 5 < 1) {
					$pagemin = 1;
					} else {
					$pagemin = $pageNum - 5;
					};
					if ($pageNum + 5 > $maxPage) {
					$pagemax = $maxPage;
					} else {
					$pagemax = $pageNum + 5;
					};
					
					for($page = $pagemin; $page <= $pagemax; $page++)
					{
					   if ($page == $pageNum)
					   {
						  $nav .= " <span class=\"currentSC\">$page</span> "; // no need to create a link to current page
					   }
					   else
					   {
						 	if(@$search)
					   		 {
							 	$nav .= " <a href=\"$self?page=$page&sname=$search\">$page</a> ";
							 }
							 else
							 {
							 	$nav .= " <a href=\"$self?page=$page\">$page</a> ";
							 }
					   }
					}
					?>
					 <?php
					if ($pageNum > 1)
					{
					   $page  = $pageNum - 1;
					   if(@$search)
					   {
						   $prev  = " <a href=\"$self?page=$page&sname=$search\">Prev</a> ";
						   $first = " <a href=\"$self?page=1&sname=$search\">First Page</a> ";
					   }
					   else
					   {
						   $prev  = " <a href=\"$self?page=$page\">Prev</a> ";
						   $first = " <a href=\"$self?page=1\">First Page</a> ";
					   }
					}
					else
					{
					   $prev  = '&nbsp;';
					   $first = '&nbsp;';
					}
					
					if ($pageNum < $maxPage)
					{
					   $page = $pageNum + 1;
					   if(@$search)
					   {
						   	 $next = " <a href=\"$self?page=$page&sname=$search\">Next</a> ";
						     $last = " <a href=\"$self?page=$maxPage&sname=$search\">Last Page</a> ";
					   }
					   else
					   {
						   	 $next = " <a href=\"$self?page=$page\">Next</a> ";
						   	 $last = " <a href=\"$self?page=$maxPage\">Last Page</a> ";
					   }
					}
					else
					{
					   $next = '&nbsp;';
					   $last = '&nbsp;';
					}
					echo $first . $prev . $nav . $next . $last;
					?>
					<div style="clear: left;"></div>
					</div>	 
				<?php
				}
                ?>
            
           <!-- ******************************************************************-->
            
          </div>
        </div>
      </div>
      
      <!-- Modal1 --><!--add option -->
      <div class="modal fade" id="myModal" tabindex="-1" role="dialog">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
              <h4 class="modal-title">PRODUCT PURCHASE</h4>
            </div>
            <div class="modal-body clearfix">
              <form action="do.php?op=index" class="form1" method="post" onsubmit="return valid()">
                <div class="row">
                  <div class="col-sm-12">
                  
                   
                    <div class="form-group">
						<label for="productCreateId">Select Product:<span class="valid">*</span></label>
						<input type="text" name="productCreateName" autocomplete="off" id="productCreateName" class="form-control2" required>
						<input type="hidden" name="productCreateId" id="productCreateId" class="form-control2" required>
						<ul id="aj_live">

						</ul>

					</div>
					<div class="form-group">
						<label for="rate">Original Rate:<span class="valid">*</span></label>	
						<input type="text" name="original_rate" id="original_rate" class="form-control2">
					</div>
					<div class="form-group">
						<label for="rate">Flavormart Rate:<span class="valid">*</span></label>	
						<input type="text" name="flavormart_rate" id="flavormart_rate" class="form-control2">
					</div>
					
                     <div class="form-group"style="border: 1px solid #ccc; display: -webkit-box; padding: 15px;">
                      <label for="productTypeId">Select Type:<span class="valid">*</span></label>
                      <ul class="category_combo_list list-unstyled" style="display: block; margin-left: 94px;">
                      <?php
                      $selectType = "select * from ".TABLE_PRODUCT_TYPE."";
                      //echo $select;
                      $resultType = $db->query($selectType);
                      $numsType = mysql_num_rows($resultType);
                      //echo $nums;
                      while($rowType=mysql_fetch_array($resultType))
                      	{
						?>
						
						<li><input type="checkbox" name="ptype[]" value="<?= $rowType['ID']; ?>">
                        <label><?= $rowType['productType']; ?></label></li>
						
						<?php
						}
						?>
						</ul>
                    </div>
                    
                    <div class="form-group" style="border: 1px solid #ccc; display: -webkit-box; padding: 15px;">
                      <label for="productTypeId">Select Category:<span class="valid">*</span></label>
                      <ul class="category_combo_list list-unstyled" style="display: block; margin-left: 60px;">
                      <?php
                      $select = "select * from ".TABLE_CATEGORIES."";
                      //echo $select;
                      $res = $db->query($select);
                      $nums = mysql_num_rows($res);
                      //echo $nums;
                      while($row=mysql_fetch_array($res))
                      	{
						?>
						
						<li><input type="checkbox" name="category[]" value="<?= $row['ID']; ?>">
                        <label><?= $row['categoryName']; ?></label></li>
						
						<?php
						}
						?>
						</ul>
                     
                    </div>
                    
                    <div class="form-group">
						<label for="homePage">Show in Home Page ?:</label>	
						<input type="radio" name="homePage" checked="checked" id="homePage" class="form-control1" value="YES"style="margin-left: 54px;">
<label style="margin-left: 5px;">Yes</label>
						<input type="radio" name="homePage" checked="checked" id="homePage" class="form-control1" value="NO" style="margin-left: 10px;">
<label style="margin-left: 5px;">No</label>
						
					</div>
                 	
					
				</div>
								
              </div>                  
                        
			  <div>
            </div>
            <div class="modal-footer">
              <input type="submit" name="save" id="save" value="SAVE" class="btn btn-primary continuebtn" />
            </div>
			</form>
          </div>
        </div>
      </div>
      <!-- Modal1 cls --> 
	 
      
  </div>
<?php include("../adminFooter.php") ?>