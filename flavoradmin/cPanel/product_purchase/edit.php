<?php include("../adminHeader.php") ?>

<?php
if($_SESSION['LogID']=="")
{
header("location:../../logout.php");
}

$db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE); 
$db->connect();
?>
<script>
function delete_type()
{
var del=confirm("Do you Want to Delete ?");
	if(del==true)
	{
	window.submit();
	}
	else
	{
	return false;
	}
}

</script>



<?php
 if(isset($_SESSION['msg'])){?><font color="red"><?php echo $_SESSION['msg']; ?></font><?php }	
 $_SESSION['msg']='';
 
	$editId=$_REQUEST['id'];
	$editId=mysql_real_escape_string($editId);
	
	$tableEditQry	=  "SELECT ".TABLE_PRODUCT_PURCHASE.".ID,
							   ".TABLE_PRODUCT_PURCHASE.".customerId,
							   ".TABLE_PRODUCT_PURCHASE.".productCreateId,
							   ".TABLE_PRODUCT_PURCHASE.".originalRate,
							   ".TABLE_PRODUCT_PURCHASE.".flavormartRate,
							   ".TABLE_PRODUCT_PURCHASE.".homePage	
							   FROM ".TABLE_PRODUCT_PURCHASE."	
						 WHERE ".TABLE_PRODUCT_PURCHASE.".ID='$editId'";
	//echo $tableEditQry;
	$tableEdit 	=	mysql_query($tableEditQry);
	$editRow	=	mysql_fetch_array($tableEdit);
	$pid		=	$editRow['productCreateId'];
	$proQry		=	mysql_query("SELECT productName FROM ".TABLE_PRODUCT_CREATION." WHERE ID=$pid");
	$proRow		= 	mysql_fetch_array($proQry);
	
	//category query 
	$qry = "SELECT ".TABLE_PURCHASE_CATEGORIES.".categoryId
								  FROM ".TABLE_PURCHASE_CATEGORIES."
							     WHERE ".TABLE_PURCHASE_CATEGORIES.".purchaseId ='$editId'";
	//echo $qry;							
	$catResult = $db->query($qry);
	$sCategory = array();
	while($catFetch = mysql_fetch_array($catResult))
	{
	 		//$sCategory = $catFetch['categoryName'];
	 		array_push($sCategory,$catFetch['categoryId']);
	}
	
	//type query
	$qryType = "SELECT ".TABLE_PURCHASE_TYPE.".typeId
								  FROM ".TABLE_PURCHASE_TYPE."
							     WHERE ".TABLE_PURCHASE_TYPE.".purchaseId ='$editId'";
	//echo $qryType;						
	$typeResult = $db->query($qryType);
	$sType = array();
	while($typeFetch = mysql_fetch_array($typeResult))
	{
	 		//$sCategory = $catFetch['categoryName'];
	 		array_push($sType,$typeFetch['typeId']);
	}	

?>

 
      <!-- Modal1 -->
      <div >
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <a class="close" href="index.php" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></a>
              <h4 class="modal-title">PRODUCT PURCHASE DETAIL</h4>
            </div>
            <div class="modal-body clearfix">
              <form action="do.php?op=edit" class="form1" method="post" onsubmit="return valid()">
			  <input type="hidden" name="id" id="id" value="<?php echo $editId ?>">
                <div class="row">
                  <div class="col-sm-6">
                  
                  <div class="form-group">
						<label for="productCreateId">Select Product:<span class="valid">*</span></label>
						<input type="text" name="productCreateName" autocomplete="off" id="productCreateName" class="form-control2" required value="<?php echo $proRow['productName'];?>">
						<input type="hidden" name="productCreateId" id="productCreateId" class="form-control2" required value="<?php echo $editRow['productCreateId'];?>">
						<ul id="aj_live">

						</ul>

					</div>
                   

                  	<div class="form-group">
                      <label for="rate">Original Rate:<span class="valid">*</span></label>
                      <input type="text" class="form-control2" name="original_rate" id="original_rate" required value="<?php echo $editRow['originalRate'];?>" >
                    </div>
                    <div class="form-group">
                      <label for="rate">Flavormart Rate:<span class="valid">*</span></label>
                      <input type="text" class="form-control2" name="flavormart_rate" id="flavormart_rate" required value="<?php echo $editRow['flavormartRate'];?>" >
                    </div>
                     <div class="form-group">
                      <label for="productTypeId">Product Type:<span class="valid">*</span></label>
                       <ul class="category_combo_list list-unstyled" style="display: block;">
                      <?php
                      
                      $select = mysql_query("select * from ".TABLE_PRODUCT_TYPE."");
                     
                      while($rowType=mysql_fetch_array($select))
                      	{
                      
						?>
						<li><input type="checkbox" name="ptype[]" value="<?= $rowType['ID']; ?>" <?php if(in_array( $rowType['ID'],$sType)){ echo "checked" ;}?>>
                        <label><?= $rowType['productType']; ?></label></li>
						
						<?php
						}
						?>
						</ul>
                    </div>
                    
                    <div class="form-group">
                      <label for="productTypeId">Select Category:<span class="valid">*</span></label>
                      
                      <ul class="category_combo_list list-unstyled" style="display: block;">
                      <?php
                      
                      $select = mysql_query("select * from ".TABLE_CATEGORIES."");
                     
                      while($row=mysql_fetch_array($select))
                      	{
                      
						?>
						<li><input type="checkbox" name="category[]" value="<?= $row['ID']; ?>" <?php if(in_array( $row['ID'],$sCategory)){ echo "checked" ;}?>>
                        <label><?= $row['categoryName']; ?></label></li>
						
						<?php
						}
						?>
						</ul>
                     
                      <!--<select class="form-control2" name="categories" id="categories" required>
                      	<option value="">Select</option>
                      	<?php
                      	$select = mysql_query("select * from ".TABLE_CATEGORIES."");
                      	while($row=mysql_fetch_array($select))
                      	{
						?>
						<option value="<?php echo $row['ID'];?>" <?php if($row['ID']==$editRow['categoryId']){ echo 'selected' ;}?>><?php echo $row['categoryName'];?></option>
						
						<?php
						}
                      	?>
                      </select>-->
                    </div>
                    
				<div class="col-sm-12">						
				
					<div class="form-group">
						<label for="homePage">Show in Home Page ?:</label>	
						<input type="radio" name="homePage" id="homePage" class="form-control1" value="YES" <?php if($editRow['homePage']=='YES'){ echo('checked');} ?> ><label>Yes</label>
						<input type="radio" name="homePage" id="homePage" class="form-control1" value="NO" <?php if($editRow['homePage']=='NO'){ echo('checked');} ?>><label>No</label>
						
						</div>									
									
				</div>				
             </div>                 
             </div>              
			  <div>
            </div>
            <div class="modal-footer">
              <input type="submit" name="save" id="save" value="UPDATE" class="btn btn-primary continuebtn" />
            </div>
			</form>
          </div>
        </div>
      </div>
      <!-- Modal1 cls --> 
     
      
  </div>
<?php include("../adminFooter.php") ?>
