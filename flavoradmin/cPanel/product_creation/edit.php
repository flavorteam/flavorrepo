<?php include("../adminHeader.php") ?>

<?php
if($_SESSION['LogID']=="")
{
header("location:../../logout.php");
}

$db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE); 
$db->connect();
?>
<script>
function delete_type()
{
var del=confirm("Do you Want to Delete ?");
	if(del==true)
	{
	window.submit();
	}
	else
	{
	return false;
	}
}

</script>

<script>
function valid()
{
flag	 	=	false;
emailReg 	= 	/^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})$/;

	email	=	document.getElementById('email').value;				
		if(!email.match(emailReg))
		{
		document.getElementById('e').innerHTML="Invalid Email address";
		flag=true;
		}
		
	userUmail	=	document.getElementById('uEmail').value;				
		if(!userUmail.match(emailReg))
		{
		document.getElementById('uEmailDiv').innerHTML="Invalid Email address";
		flag=true;
		}
		
	password	=	document.getElementById('password').value;
	cPassword	=	document.getElementById('cPassword').value;
		if(password != cPassword)
		{
			document.getElementById('pwdDiv').innerHTML = 'Password not matching'
			flag	=	true;
		}
			
	if(flag==true)
	{
	return false;
	}																			
}

//clear the validation msg
function clearbox(Element_id)
{
document.getElementById(Element_id).innerHTML="";
}
</script>

<?php
 if(isset($_SESSION['msg'])){?><font color="red"><?php echo $_SESSION['msg']; ?></font><?php }	
 $_SESSION['msg']='';

	$editId=$_REQUEST['id'];
	$editId=mysql_real_escape_string($editId);
	$tableEditQry	=  "SELECT *						  
						  FROM ".TABLE_PRODUCT_CREATION."						  
						 WHERE ".TABLE_PRODUCT_CREATION.".ID='$editId'";
	
	$tableEdit 	=	mysql_query($tableEditQry);
	$editRow	=	mysql_fetch_array($tableEdit);
?>

 
      <!-- Modal1 -->
      <div >
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <a class="close" href="index.php" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></a>
              <h4 class="modal-title">PRODUCT CREATION </h4>
            </div>
            <div class="modal-body clearfix">
              <form action="do.php?op=edit" class="form1" method="post" onsubmit="return valid()">
			  <input type="hidden" name="id" id="id" value="<?php echo $editId ?>">			  
                <div class="row">
                  <div class="col-sm-6">						
                    <div class="form-group">
						<label for="name">Product Name:<span class="valid">*</span></label>	
						<input type="text" name="productName" id="productName" class="form-control2" required value="<?php echo $editRow['productName'];?>">
					</div>
					 <div class="form-group">
						<label for="description">Description:</label>
						<textarea name="description" id="description" class="form-control2"  style="height:80%"><?php echo $editRow['description'];?></textarea>
					</div>
					<div class="form-group">
						<label for="features">Features:</label>	
						<input type="text" name="features" id="tinyText" class="form-control2"  value="<?php echo $editRow['features'];?>">
					</div>
				</div>
				                   							
								
             </div>                 
             </div>              
			  <div>
            </div>
            <div class="modal-footer">
              <input type="submit" name="save" id="save" value="UPDATE" class="btn btn-primary continuebtn" />
            </div>
			</form>
          </div>
        </div>
      </div>
      
      <script>
  tinymce.init({
    selector: '#tinyText'
  });
  </script>
      <!-- Modal1 cls --> 
     
      
  </div>
<?php include("../adminFooter.php") ?>
