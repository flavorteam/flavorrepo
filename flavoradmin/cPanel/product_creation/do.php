<?php
require("../../config/config.inc.php"); 
require("../../config/Database.class.php");
require("../../config/Application.class.php");

if($_SESSION['LogID']=="")
{
header("location:../../logout.php");
}
$loginId	=	$_SESSION['LogID'];
$loginType	=	$_SESSION['LogType'];
$optype=(strtolower(empty($_POST['op']))) ? ((strtolower(empty($_GET['op']))) ? $_REQUEST['op'] : $_GET['op']) : $_POST['op'];

switch($optype)
{
	// NEW SECTION
	case 'index':
		
		if(!$_REQUEST['productName'])
			{
				$_SESSION['msg']="Error, Invalid Details!";			
				header("location:index.php");		
			}
			
		else
			{				
				$db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE); 
				$db->connect();
				$success1=0;							
				
				$name		=	$App->convert($_REQUEST['productName']);
				$name		=	ucwords(strtolower($name));
					
				
				$existId=$db->existValuesId(TABLE_PRODUCT_CREATION,"productName='$name'");
					if($existId>0)
					{
						$_SESSION['msg']="Product Is Already Exist";													
					}
					else
					{	
										
						$data['productName']			=	$name;
						$data['description']	=	ucfirst(strtolower($App->convert($_REQUEST['description'])));
						$data['features']	=	$App->convert($_REQUEST['features']);	
						$success1=$db->query_insert(TABLE_PRODUCT_CREATION,$data);
									
						$db->close();
						
							if($success1)
							{							
								$_SESSION['msg']="Product Details Added Successfully";										
							}
							
							else{
								$_SESSION['msg']="Failed";
							}
					}	
									
				header("location:index.php");											
			}
					
		break;		
	// EDIT SECTION
	case 'edit':		
		$editId	=	$_REQUEST['id']; 
			    	
		if(!$_REQUEST['productName'])
			{
				$_SESSION['msg']="Error, Invalid Details!";					
				header("location:index.php");		
			}
		else
			{				
				$db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE); 
				$db->connect();
				$success1=0;
								
				$name		=	$App->convert($_REQUEST['productName']);
				$name		=	ucwords(strtolower($name));
				
				$existId=$db->existValuesId(TABLE_PRODUCT_CREATION,"productName='$name' and ID!='$editId'");
					if($existId>0)
					{
						$_SESSION['msg']="Product Is Already Exist";													
					}
					else
					{						
						$data['productName']			=	$name;
						$data['description']	=	ucfirst(strtolower($App->convert($_REQUEST['description'])));
						$data['features']		=	ucfirst(strtolower($App->convert($_REQUEST['features'])));					
					
						$success1=$db->query_update(TABLE_PRODUCT_CREATION,$data," ID='{$editId}'");
									
						$db->close();
						
							if($success1)
							{							
								$_SESSION['msg']="Product Details Updated Successfully";										
							}
							else{
								$_SESSION['msg']="Failed";
							}
					}					
				header("location:index.php");
														
			}	
			break;	
				
	case 'delete':
			$id = $_REQUEST['id'];	
			$db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE);	
			$db->connect();	
			
			/*$selectProModel = "select * from `".TABLE_PRODUCT_CATEGORY."` where productID = $id";
			$resProModel = mysql_query($selectProModel);
			$i = 0;
			while($rowProModel = mysql_fetch_array($resProModel))
			{
				$modelImageArray[$i++] = $rowProModel['modelImagePath'];
			}
			for($i=0; $i<count($modelImageArray); $i++)
			{
				unlink('../../'.$modelImageArray[$i]);
			}*/
			/*$db->query("DELETE FROM `".TABLE_PRODUCT_CATEGORY."` WHERE productID='{$id}'");	*/
			
			//$selectThumbDel = "select * from ".TABLE_PRODUCT_CREATION." where ID=".$id;
			
			@mysql_query("DELETE FROM `".TABLE_PRODUCT_CREATION."` WHERE ID='{$id}'");
			@mysql_query("DELETE FROM `".TABLE_PRODUCT_PURCHASE."` WHERE productCreateId='{$id}'");								
			$db->close(); 
			$_SESSION['msg']="Product Deleted Successfully";					
			header("location:index.php");	
						
		break;
}
?>