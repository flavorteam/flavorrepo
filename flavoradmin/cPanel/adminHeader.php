<?php 
require("../../config/config.inc.php"); 
require("../../config/Database.class.php");
require("../../config/Application.class.php");
$loginId= $_SESSION['LogID'];
$loginType= $_SESSION['LogType'];
?>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>FlavorMart</title>
<script type="text/javascript" src="../../js/jquery.js"></script>
<script type="text/javascript" src="../../js/jquery_pagination.js"></script>
<script type="text/javascript" src="../../js/jquery-2.1.4.min.js"></script>

<script src='https://cdn.tinymce.com/4/tinymce.min.js'></script>
	
<!-- Bootstrap -->
<link href="../../css/bootstrap.min.css" rel="stylesheet">
<link href="../../css/style.css" rel="stylesheet">
<link href="../../css/newStyle.css" rel="stylesheet">
<link href="../../css/jQuery-pagination.css" rel="stylesheet">
<link href="../../font-awesome/css/font-awesome.min.css" rel="stylesheet">
<link href="../../css/bootstrap-datepicker.min.css" rel="stylesheet">
<link href='http://fonts.googleapis.com/css?family=Roboto:400,500,700,300,900' rel='stylesheet' type='text/css'>
<link href="../../css/owl.carousel.css" rel="stylesheet" />

<!-- timepicker -->
<link rel="stylesheet" type="text/css" href="../../css/bootstrap-datetimepicker.min.css">

</head>
<body class="bgmain">
<div id="header">
  <div class="container"> <a href="#" class="logo"></a>
    <div class="navigation">
      <div class="dropdown"> <a id="dLabel" data-target="#" href="#" data-toggle="dropdown" aria-haspopup="true" role="button" aria-expanded="false"> Settings <span class="caret"></span> </a>
        <ul class="dropdown-menu" role="menu" aria-labelledby="dLabel">
          <li><a href="../changePassword/new.php">Change Password</a></li>
          <li><a href="../../logout.php">Logout</a></li>
        </ul>
      </div>
    </div>
  </div>
  <div class="clearer"></div>
</div>
<div id="contentarea">
  <div class="container">
    <div class="row">
      <div class="col-md-2 col-sm-4 leftnav">
        <div class="leftmenu">
          <ul>
            <!--<li> <a href="#" class="dropdown-toggle" > <span class="menuicons icn1"></span> Home </a></li>-->
            <?php
            if($loginType=='Admin')
            {
            ?>

            <li> <a href="../category/index.php" class="dropdown-toggle"> <span class="menuicons icn10"></span>Categories</a></li>
            <li> <a href="../product_type/index.php" class="dropdown-toggle"> <span class="menuicons icn4"></span>Product Type</a></li>
            <li> <a href="../product_creation/index.php" class="dropdown-toggle"> <span class="menuicons icn8"></span>Product Creation</a></li>
        	<li> <a href="../product_purchase/index.php" class="dropdown-toggle"> <span class="menuicons icn10"></span>Product Purchase</a></li>
        	 
        	<li> <a href="../customer/index.php" class="dropdown-toggle"> <span class="menuicons icn8"></span>User Registration</a></li>
        	<li> <a href="../shippingDetails/index.php" class="dropdown-toggle"> <span class="menuicons icn8"></span>Shipping Details</a></li>
        		<li> <a href="../cart/index.php" class="dropdown-toggle"> <span class="menuicons icn8"></span>Cart</a></li>
        		<li> <a href="../purchase_table/index.php" class="dropdown-toggle"> <span class="menuicons icn8"></span>Purchase Report</a></li>
        		<li> <a href="../banner_image/index.php" class="dropdown-toggle"> <span class="menuicons icn10"></span>Banner Image</a></li> 
        		<li> <a href="../inner_banner_image/index.php" class="dropdown-toggle"> <span class="menuicons icn10"></span>Inner Banner Image</a></li>
        		
        	        
            
           <?php  
			}
			?>
                               		 
		 </ul>
        </div>
      </div>