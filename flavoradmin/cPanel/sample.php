<?php
session_start();
$products = array(
    array('SKU'=>test1, 'name'=>ProductTest1, 'Price'=>10.00),
    array('SKU'=>test2, 'name'=>ProductTest2, 'Price'=>11.00),
    array('SKU'=>test3, 'name'=>ProductTest3, 'Price'=>12.00),
);

if (isset($_GET['action'] && $_GET['action'] === 'sample') {
   if (!isset($_SESSION['cart']) $_SESSION['cart'] = array();
   $_SESSION['cart'][] = $_GET['product'];
}

?>

<html>
<head>
<link rel="stylesheet" type="text/css" href="Style.css">
</head>
<body>
<div id="container">
<div id="main">
<?php if (isset($_SESSION['cart']) && !empty($_SESSION['cart'])): ?>
<ul id="cart">
<?php foreach($_SESSION['cart'] as $product): ?>
   <li><?= $products[$product]['name'] ?></li>
<?php endforeach; ?>
</ul>
<?php endif; ?>

<table>
<tr>
<th>SKU</th>
<th>Name</th>
<th>Price</th>
<th>Action</th>
</tr>
<?php foreach ($products as $key => $product): ?>
<tr>
<td><?php echo $product['SKU']; ?></td>
<td><?php echo $product['name']; ?></td>
<td><?php echo '�'. number_format($product['Price'],2); ?></td>
<td><a href="?action=addToCart&product=<?php echo $key ?>">Add To Cart</a></td>
</tr>
<?php endif; ?>
</table>
<input id="Proceed" type="Submit" value="Proceed">
</div>
</div>
</body>
</html>