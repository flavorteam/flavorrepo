<?php
require("../../config/config.inc.php");
require("../../config/Database.class.php");
require("../../config/Application.class.php");
header('Content-type: Application/json');
if (isset($_REQUEST['searchKey'])) {
    $searchKey = $_REQUEST['searchKey'];
    $db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE);
    $db->connect();
    $productQry = "SELECT ID, productName FROM `".TABLE_PRODUCT_CREATION."` WHERE productName LIKE '%".$searchKey."%'";
    $productRes = mysql_query($productQry);
    if (mysql_num_rows($productRes) > 0) {
        $productCount = mysql_num_rows($productRes);
        $productNames = array();
        while ($productRow = mysql_fetch_array($productRes)) {
            $tempArray = array("id" => $productRow['ID'], 'product_name' => $productRow['productName']);
            array_push($productNames, $tempArray);
        }
        $result = array("status" => "ok", "count" => $productCount, "product_list" => $productNames);
    } else {
        $result = array("status" => "failure", "error" => "No products found!");
    }
} else {
    $result = array("status" => "failure", "error" => "No search keyword!");
}
echo json_encode($result);
?>