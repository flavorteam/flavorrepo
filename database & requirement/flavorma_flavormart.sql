-- phpMyAdmin SQL Dump
-- version 4.6.6
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: May 29, 2017 at 08:22 AM
-- Server version: 5.5.54-cll-lve
-- PHP Version: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `flavorma_flavormart`
--

-- --------------------------------------------------------

--
-- Table structure for table `banner_image`
--

CREATE TABLE `banner_image` (
  `ID` int(11) NOT NULL,
  `image` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `banner_image`
--

INSERT INTO `banner_image` (`ID`, `image`) VALUES
(1, 'banner_uploads/20160616181819.jpg'),
(2, 'banner_uploads/20160617084757.jpg'),
(3, 'banner_uploads/20160620153347.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `ID` int(11) NOT NULL,
  `categoryName` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`ID`, `categoryName`) VALUES
(1, 'OILS & FATS'),
(2, 'FLAVOURS & COLOURS'),
(6, 'DRY & WET FRUITS'),
(7, 'CHOCLATES & CAKE'),
(8, 'JAMS & GELS'),
(9, 'CRUSHES & SAUCES'),
(10, 'FRUITIES & FILLINGS'),
(11, 'DRINKS & DAIRY'),
(12, 'MASALAS & POWDERS'),
(13, 'PACKING MATERIALS'),
(14, 'OTHER INGREDIENTS');

-- --------------------------------------------------------

--
-- Table structure for table `customer`
--

CREATE TABLE `customer` (
  `ID` int(11) NOT NULL,
  `cusName` varchar(20) NOT NULL,
  `email` varchar(50) NOT NULL,
  `address` varchar(100) NOT NULL,
  `phone` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `inner_banner_image`
--

CREATE TABLE `inner_banner_image` (
  `ID` int(11) NOT NULL,
  `image` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `login`
--

CREATE TABLE `login` (
  `ID` int(11) NOT NULL,
  `staffId` int(11) NOT NULL,
  `userName` varchar(100) NOT NULL,
  `password` varchar(200) NOT NULL,
  `type` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `login`
--

INSERT INTO `login` (`ID`, `staffId`, `userName`, `password`, `type`) VALUES
(21, 0, 'admin', '0be838ae9ee94e05fed0925454f8f44b', 'Admin'),
(30, 1, 'user1', '24c9e15e52afc47c225b757e7bee1f9d', 'user'),
(31, 2, 'Sruthi', 'd14ceb37e82ddfe68777e8454997ed7d', 'user'),
(32, 3, 'new', '22af645d1859cb5ca6da0c484f1f37ea', 'user'),
(33, 4, 'suju', '0870ee638d51d09f8f299e7356a48f36', 'user'),
(34, 5, 'a', '0cc175b9c0f1b6a831c399e269772661', 'user');

-- --------------------------------------------------------

--
-- Table structure for table `product_creation`
--

CREATE TABLE `product_creation` (
  `ID` int(11) NOT NULL,
  `productName` varchar(100) NOT NULL,
  `description` varchar(100) NOT NULL,
  `features` varchar(5000) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `product_creation`
--

INSERT INTO `product_creation` (`ID`, `productName`, `description`, `features`) VALUES
(12, 'Sample Product', 'This is a sample product.', '<p>&nbsp;</p><p>Description about sample product for flavormart. This is new product added by admin.</p>'),
(13, 'Whip Topping', 'Premium whip topping cream 1kg is available', ''),
(14, 'Compound (mango Flavour)', '', ''),
(15, 'White Forest Cake Mix', '', ''),
(16, 'Desire Chocolate Flavour', 'Cake mix - chocolate flavours', ''),
(17, 'Chocolate Glaze', 'Crustogel chocolate\r\n\r\nchocolate glaze', ''),
(18, 'Whipping Cream Powder', 'Bakers whip\r\nwhipping cream powder', ''),
(19, 'Truffle Base', 'Truffle made easy', ''),
(20, 'Mala\'s Crush', 'Blueberry, mango, pineapple, strawberry, litchi, orange and green apple', ''),
(21, 'Skol', 'Topping syrup-mango, pista, pineapple', ''),
(22, 'Milk Maid', '', ''),
(23, 'Rasgulla', '', ''),
(24, 'Fruit Drink', 'Apple,mango,green apple, water milon', ''),
(25, 'Gulab Jamun', '', ''),
(26, 'Streilized Paneer', '', ''),
(27, 'Mayonnaise', 'Eggless', ''),
(28, 'Walnuts', '', ''),
(29, 'Fraji Dates', '', ''),
(30, 'Pista', '', ''),
(31, 'Badam', '', ''),
(32, 'Cashew Nut', '', ''),
(33, 'Snow Peak', 'Red cherry', ''),
(34, 'Dara Stem Red Cherry', '', ''),
(35, 'Dry Figs', '', ''),
(36, 'Emperor Dates', '', ''),
(37, 'Cashewnut Bb2', '', ''),
(38, 'Dates', '', ''),
(39, 'Nutmeg', '', ''),
(40, 'Aldia', '', ''),
(41, 'Dry Dates', '', ''),
(42, 'Prunes', '', ''),
(43, 'Del Monte Slices', 'Pineapple slices, fruit coktail, red cherries', ''),
(44, 'Fruitoman Pulps', 'Mango pulps and tomato puree', ''),
(45, 'Olives', '', ''),
(46, 'Symega Vanilla Special', '', ''),
(47, 'Symega Pineapple #1 Flavour', '', ''),
(48, 'Symega  Chocolate Special', '', ''),
(49, 'Milk Dairy Flv', '', ''),
(50, 'Liquid Food Colour', 'Blue , pink , chocolate', ''),
(51, 'Aromatic Food Colour', '', ''),
(52, 'Food Colour Powder', '', ''),
(53, 'Food Colour Tartrazine', 'Food colour', ''),
(54, 'Bush Orange Red Powder', 'Food colour', ''),
(55, 'Food Colour Lemon Yellow', '', ''),
(56, 'Milky Flavours', 'Pista elachi, strawberry, mango, badam', ''),
(58, 'Fruitoman\'s Flavour', 'Pineapple, vanilla, ice cream', ''),
(59, 'Vanilla Powder Popular', '', ''),
(60, 'Food Colour Orange Red', '', ''),
(61, 'Vanillapowder French Deluxe', '', ''),
(62, 'Vanilla Powder French Deluxe', '', ''),
(63, 'Fruit Flavoured Filling Strawberry', '', ''),
(64, 'Topping Sauce', 'Apple, orange, kiwi', ''),
(65, 'Caramel Topping', '', ''),
(66, 'Dessert Topping', 'Irish coffee  and chocolate', ''),
(67, 'Chocolate Syrup,  Date Syrup', '', ''),
(68, 'Fruit Flavoured Pineapple', '', ''),
(69, 'White Glaze', '', ''),
(70, 'Strawberry Cold Glaze', '', ''),
(71, 'Orange Cold Glaze', '', ''),
(72, 'Glamour Cold Glaze', '', ''),
(73, 'Cake Jel', '', ''),
(74, 'Piping Jelly', '', ''),
(75, 'Cremfil Silk', '', ''),
(76, 'Purix Cake Gel', '', ''),
(77, 'Chocolate Cold Glaze', '', ''),
(78, 'Jelly Crystals', 'Starwberry, pineapple, orange, litchi', ''),
(79, 'Purix Mango Gel', '', ''),
(80, 'Purix Kiwi Gel', '', ''),
(81, 'Purix Butterscotch Gel', '', ''),
(82, 'Fruitoman\'s Sauce', 'Red chilly sauce, green chilly sauce, soyabean sauce, sarsaparilla syrup.', ''),
(83, 'Fruitoman\'s Jam', 'Pineapple jam and mixed fruit jam', ''),
(84, 'Palat Biriyani Masala', '', ''),
(85, 'Fruitoman\'s', 'Custard powder, channa masala, caramel pudding mix, corn flour.', ''),
(86, 'Fruitoman\'s Chat Masala N Kasoori Methi', '', ''),
(87, 'Fruitoman\'s', 'Ice cream powder, china grass, active dried yeast, gelatine, cocoa powder.', ''),
(88, 'Plain China Grass, And Kasuri Methi', '', ''),
(89, 'Olive Pomace Oil', '', ''),
(90, 'Rkg Ghee', '', ''),
(91, 'Sunrich Sunflower Oil', '', ''),
(92, 'Lily Refined Sunflower Oil', '', ''),
(93, 'Ghee (gsm Classic)', '', ''),
(94, 'Ghee (ssm Magic)', '', ''),
(95, 'Ghee (sheel Vanaspati)', '', ''),
(96, 'Ruchi Gold Mustard Oil', '', ''),
(97, 'Ricca Premium Vanaspati', '', ''),
(98, 'Purix Citric Acid', '', ''),
(99, 'Green Sugar Paste', '', ''),
(100, 'Cks', 'Vanilla powder, gelatin, sorbic acid, and gurgum.', ''),
(101, 'Purix Pectin', '', ''),
(102, 'Liquid Glucose', '', ''),
(103, 'Acetic Acid', '', ''),
(104, 'Food Coloring Caramel', '', ''),
(105, 'Golden Syrup', '', ''),
(106, 'Ice Cream Mix Powder', '', ''),
(107, 'Crust N Crumb', 'Crispy chicken mix, brownie mix, pan cake mix, custard powder, and  corn flour.', ''),
(108, 'Instant Dry Yeast', '', ''),
(109, 'Deco Sprinkels', '', ''),
(110, 'Fancy Strands', '', ''),
(111, 'Flower', '', ''),
(112, 'Yoddha Tooth Picks', '', ''),
(113, 'Tomato Ketchup', '', ''),
(114, 'Pure Arrowroot Powder', '', ''),
(115, 'Chukku Kappi', '', ''),
(116, 'Cake Turntable', '', ''),
(117, 'Clip', '', ''),
(118, 'Cup Cake Cover', '', ''),
(119, 'Knife', '', ''),
(120, 'Silcone Whisk', '', ''),
(121, 'Cake Container', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `product_pic`
--

CREATE TABLE `product_pic` (
  `ID` int(11) NOT NULL,
  `productId` int(11) NOT NULL,
  `picture` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `product_pic`
--

INSERT INTO `product_pic` (`ID`, `productId`, `picture`) VALUES
(4, 3, 'productPhoto/20160704115458.jpg'),
(7, 4, 'productPhoto/20160704123019.jpg'),
(9, 7, 'productPhoto/dummy.jpg'),
(13, 8, 'productPhoto/20160704132638.jpg'),
(14, 9, 'productPhoto/20160704132725.jpg'),
(15, 6, 'productPhoto/20160704132851.jpg'),
(16, 10, 'productPhoto/20160704132956.jpg'),
(23, 11, 'productPhoto/dummy.jpg'),
(24, 5, 'productPhoto/20170123162437.jpg'),
(27, 12, 'productPhoto/20170125095523.jpg'),
(28, 13, 'productPhoto/20170125095545.jpg'),
(30, 14, 'productPhoto/20170125095819.jpg'),
(32, 15, 'productPhoto/20170125100043.jpg'),
(34, 16, 'productPhoto/20170125100253.jpg'),
(36, 17, 'productPhoto/20170125100448.jpg'),
(39, 18, 'productPhoto/20170125100640.jpg'),
(40, 18, 'productPhoto/20170125100652.jpg'),
(41, 12, 'productPhoto/20170125100706.jpg'),
(45, 19, 'productPhoto/20170125101045.jpg'),
(46, 19, 'productPhoto/20170125101054.jpg'),
(48, 20, 'productPhoto/20170125101232.jpg'),
(50, 21, 'productPhoto/20170125101337.jpg'),
(52, 22, 'productPhoto/20170125101438.jpg'),
(54, 23, 'productPhoto/20170125101557.jpg'),
(56, 24, 'productPhoto/20170125101715.jpg'),
(58, 25, 'productPhoto/20170125101831.jpg'),
(60, 26, 'productPhoto/20170125102004.jpg'),
(62, 27, 'productPhoto/20170125102116.jpg'),
(64, 28, 'productPhoto/20170125102402.jpg'),
(66, 29, 'productPhoto/20170125102647.jpg'),
(68, 30, 'productPhoto/20170125102834.jpg'),
(70, 31, 'productPhoto/20170125102931.jpg'),
(72, 32, 'productPhoto/20170125103130.jpg'),
(74, 33, 'productPhoto/20170125103250.jpg'),
(75, 33, 'productPhoto/20170125103324.jpg'),
(76, 32, 'productPhoto/20170125103334.jpg'),
(78, 34, 'productPhoto/20170125103627.jpg'),
(80, 35, 'productPhoto/20170125103804.jpg'),
(82, 36, 'productPhoto/20170125103858.jpg'),
(84, 37, 'productPhoto/20170125103957.jpg'),
(86, 38, 'productPhoto/20170125104602.jpg'),
(88, 39, 'productPhoto/20170125104853.jpg'),
(90, 40, 'productPhoto/20170125105001.jpg'),
(92, 41, 'productPhoto/20170125105114.jpg'),
(94, 42, 'productPhoto/20170125105337.jpg'),
(96, 43, 'productPhoto/20170125105510.jpg'),
(98, 44, 'productPhoto/20170125105638.jpg'),
(100, 45, 'productPhoto/20170125111028.jpg'),
(102, 46, 'productPhoto/20170125111223.jpg'),
(104, 47, 'productPhoto/20170125111400.jpg'),
(106, 48, 'productPhoto/20170125111519.jpg'),
(108, 49, 'productPhoto/20170125111750.jpg'),
(110, 50, 'productPhoto/20170125111915.jpg'),
(112, 51, 'productPhoto/20170125112124.jpg'),
(114, 52, 'productPhoto/20170125112308.jpg'),
(115, 51, 'productPhoto/20170125112415.jpg'),
(117, 53, 'productPhoto/20170125112626.jpg'),
(118, 53, 'productPhoto/20170125112708.jpg'),
(120, 54, 'productPhoto/20170125113753.jpg'),
(122, 55, 'productPhoto/20170125113928.jpg'),
(124, 56, 'productPhoto/20170125114221.jpg'),
(125, 56, 'productPhoto/20170125114340.jpg'),
(127, 57, 'productPhoto/20170125114507.jpg'),
(129, 58, 'productPhoto/20170125114633.jpg'),
(131, 59, 'productPhoto/20170125114903.jpg'),
(133, 60, 'productPhoto/20170125115029.jpg'),
(135, 61, 'productPhoto/20170125120448.jpg'),
(137, 62, 'productPhoto/20170125120634.jpg'),
(139, 63, 'productPhoto/20170125120756.jpg'),
(141, 64, 'productPhoto/20170125120912.jpg'),
(143, 65, 'productPhoto/20170125121017.jpg'),
(145, 66, 'productPhoto/20170125121624.jpg'),
(147, 67, 'productPhoto/20170125121722.jpg'),
(149, 68, 'productPhoto/20170125121844.jpg'),
(151, 69, 'productPhoto/20170125121931.jpg'),
(152, 68, 'productPhoto/20170125121959.jpg'),
(154, 70, 'productPhoto/20170125122101.jpg'),
(156, 71, 'productPhoto/20170125122146.jpg'),
(158, 72, 'productPhoto/20170125122244.jpg'),
(160, 73, 'productPhoto/20170125122354.jpg'),
(162, 74, 'productPhoto/20170125122505.jpg'),
(164, 75, 'productPhoto/20170125122601.jpg'),
(166, 76, 'productPhoto/20170125122709.jpg'),
(168, 77, 'productPhoto/20170125122827.jpg'),
(172, 80, 'productPhoto/20170125123322.jpg'),
(173, 80, 'productPhoto/20170125123351.jpg'),
(175, 81, 'productPhoto/20170125123504.jpg'),
(176, 79, 'productPhoto/20170125123541.jpg'),
(177, 78, 'productPhoto/20170125123604.jpg'),
(179, 82, 'productPhoto/20170125123820.jpg'),
(181, 83, 'productPhoto/20170125124019.jpg'),
(183, 84, 'productPhoto/20170125124140.jpg'),
(186, 85, 'productPhoto/20170125124352.jpg'),
(188, 86, 'productPhoto/20170125124444.jpg'),
(190, 87, 'productPhoto/20170125125105.jpg'),
(192, 88, 'productPhoto/20170125125207.jpg'),
(194, 89, 'productPhoto/20170125125327.jpg'),
(196, 90, 'productPhoto/20170125125438.jpg'),
(198, 91, 'productPhoto/20170125125607.jpg'),
(200, 92, 'productPhoto/20170125125731.jpg'),
(202, 93, 'productPhoto/20170125125846.jpg'),
(204, 94, 'productPhoto/20170125125950.jpg'),
(206, 95, 'productPhoto/20170125130142.jpg'),
(208, 96, 'productPhoto/20170125163430.jpg'),
(210, 97, 'productPhoto/20170125163818.jpg'),
(212, 98, 'productPhoto/20170125164052.jpg'),
(214, 99, 'productPhoto/20170125164307.jpg'),
(217, 100, 'productPhoto/20170125164425.jpg'),
(219, 101, 'productPhoto/20170125164629.jpg'),
(221, 102, 'productPhoto/20170217101934.jpg'),
(225, 103, 'productPhoto/20170217102110.jpg'),
(227, 104, 'productPhoto/20170217102300.jpg'),
(229, 105, 'productPhoto/20170217102414.jpg'),
(230, 105, 'productPhoto/20170217102422.jpg'),
(231, 105, 'productPhoto/20170217102439.jpg'),
(233, 106, 'productPhoto/20170217102558.jpg'),
(235, 107, 'productPhoto/20170217102734.jpg'),
(237, 108, 'productPhoto/20170217102842.jpg'),
(239, 109, 'productPhoto/20170217102932.jpg'),
(241, 110, 'productPhoto/20170217103018.jpg'),
(243, 111, 'productPhoto/20170217103120.jpg'),
(245, 112, 'productPhoto/20170217103218.jpg'),
(247, 113, 'productPhoto/20170217103336.jpg'),
(249, 114, 'productPhoto/20170217103432.jpg'),
(253, 116, 'productPhoto/20170217103854.jpg'),
(254, 116, 'productPhoto/20170217103901.jpg'),
(256, 115, 'productPhoto/20170217103914.jpg'),
(257, 115, 'productPhoto/20170217103921.jpg'),
(259, 117, 'productPhoto/20170217104013.jpg'),
(261, 118, 'productPhoto/20170217104132.jpg'),
(262, 116, 'productPhoto/20170217104152.jpg'),
(264, 119, 'productPhoto/20170217104252.jpg'),
(265, 119, 'productPhoto/20170217104259.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `product_purchase`
--

CREATE TABLE `product_purchase` (
  `ID` int(11) NOT NULL,
  `customerId` int(11) NOT NULL,
  `productCreateId` varchar(20) NOT NULL,
  `originalRate` int(20) NOT NULL,
  `flavormartRate` int(20) NOT NULL,
  `homePage` varchar(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `product_purchase`
--

INSERT INTO `product_purchase` (`ID`, `customerId`, `productCreateId`, `originalRate`, `flavormartRate`, `homePage`) VALUES
(11, 0, '12', 0, 0, 'YES'),
(12, 0, '13', 0, 0, 'YES'),
(13, 0, '14', 0, 0, 'NO'),
(14, 0, '15', 0, 0, 'YES'),
(15, 0, '16', 0, 0, 'NO'),
(16, 0, '17', 0, 0, 'YES'),
(17, 0, '18', 0, 0, 'YES'),
(18, 0, '19', 0, 0, 'NO'),
(19, 0, '20', 0, 0, 'YES'),
(20, 0, '21', 0, 0, 'YES'),
(21, 0, '22', 0, 0, 'YES'),
(22, 0, '23', 0, 0, 'NO'),
(23, 0, '24', 0, 0, 'YES'),
(24, 0, '25', 0, 0, 'YES'),
(25, 0, '26', 0, 0, 'NO'),
(26, 0, '27', 0, 0, 'NO'),
(27, 0, '28', 0, 0, 'NO'),
(28, 0, '29', 0, 0, 'YES'),
(29, 0, '30', 0, 0, 'YES'),
(30, 0, '31', 0, 0, 'YES'),
(31, 0, '32', 0, 0, 'NO'),
(32, 0, '33', 0, 0, 'NO'),
(33, 0, '34', 0, 0, 'NO'),
(34, 0, '35', 0, 0, 'NO'),
(35, 0, '36', 0, 0, 'NO'),
(36, 0, '37', 0, 0, 'NO'),
(37, 0, '38', 0, 0, 'NO'),
(38, 0, '39', 0, 0, 'NO'),
(39, 0, '40', 0, 0, 'NO'),
(40, 0, '41', 0, 0, 'NO'),
(41, 0, '42', 0, 0, 'YES'),
(42, 0, '43', 0, 0, 'YES'),
(43, 0, '44', 0, 0, 'NO'),
(44, 0, '45', 0, 0, 'YES'),
(45, 0, '46', 0, 0, 'YES'),
(46, 0, '47', 0, 0, 'NO'),
(47, 0, '48', 0, 0, 'NO'),
(48, 0, '49', 0, 0, 'NO'),
(49, 0, '50', 0, 0, 'NO'),
(50, 0, '51', 0, 0, 'YES'),
(51, 0, '52', 0, 0, 'NO'),
(52, 0, '53', 0, 0, 'YES'),
(53, 0, '54', 0, 0, 'YES'),
(54, 0, '55', 0, 0, 'YES'),
(55, 0, '56', 0, 0, 'NO'),
(56, 0, '58', 0, 0, 'NO'),
(57, 0, '59', 0, 0, 'YES'),
(58, 0, '60', 0, 0, 'NO'),
(59, 0, '61', 0, 0, 'YES'),
(60, 0, '62', 0, 0, 'NO'),
(61, 0, '63', 0, 0, 'YES'),
(62, 0, '64', 0, 0, 'NO'),
(63, 0, '65', 0, 0, 'YES'),
(64, 0, '66', 0, 0, 'NO'),
(65, 0, '67', 0, 0, 'NO'),
(66, 0, '68', 0, 0, 'YES'),
(67, 0, '69', 0, 0, 'YES'),
(68, 0, '70', 0, 0, 'NO'),
(69, 0, '71', 0, 0, 'NO'),
(70, 0, '72', 0, 0, 'NO'),
(71, 0, '73', 0, 0, 'NO'),
(72, 0, '74', 0, 0, 'NO'),
(73, 0, '75', 0, 0, 'NO'),
(74, 0, '76', 0, 0, 'NO'),
(75, 0, '77', 0, 0, 'NO'),
(76, 0, '78', 0, 0, 'NO'),
(77, 0, '79', 0, 0, 'NO'),
(78, 0, '80', 0, 0, 'NO'),
(79, 0, '81', 0, 0, 'NO'),
(80, 0, '82', 0, 0, 'NO'),
(81, 0, '83', 0, 0, 'NO'),
(82, 0, '84', 0, 0, 'YES'),
(83, 0, '85', 0, 0, 'NO'),
(84, 0, '86', 0, 0, 'NO'),
(85, 0, '87', 0, 0, 'NO'),
(86, 0, '88', 0, 0, 'NO'),
(87, 0, '89', 0, 0, 'NO'),
(88, 0, '90', 0, 0, 'YES'),
(89, 0, '91', 0, 0, 'NO'),
(90, 0, '92', 0, 0, 'NO'),
(91, 0, '93', 0, 0, 'NO'),
(92, 0, '94', 0, 0, 'NO'),
(93, 0, '95', 0, 0, 'YES'),
(94, 0, '96', 0, 0, 'YES'),
(95, 0, '97', 0, 0, 'YES'),
(96, 0, '98', 0, 0, 'YES'),
(97, 0, '99', 0, 0, 'YES'),
(98, 0, '100', 0, 0, 'YES'),
(99, 0, '101', 0, 0, 'NO'),
(100, 0, '102', 0, 0, 'YES'),
(101, 0, '103', 0, 0, 'NO'),
(102, 0, '104', 0, 0, 'NO'),
(103, 0, '105', 0, 0, 'YES'),
(104, 0, '106', 0, 0, 'NO'),
(105, 0, '107', 0, 0, 'YES'),
(106, 0, '108', 0, 0, 'YES'),
(107, 0, '109', 0, 0, 'YES'),
(108, 0, '110', 0, 0, 'NO'),
(109, 0, '111', 0, 0, 'NO'),
(110, 0, '112', 0, 0, 'NO'),
(111, 0, '113', 0, 0, 'YES'),
(112, 0, '114', 0, 0, 'NO'),
(113, 0, '115', 0, 0, 'YES'),
(114, 0, '116', 0, 0, 'YES'),
(115, 0, '117', 0, 0, 'NO'),
(116, 0, '118', 0, 0, 'NO'),
(117, 0, '119', 0, 0, 'NO'),
(118, 0, '120', 0, 0, 'NO'),
(119, 0, '121', 0, 0, 'NO');

-- --------------------------------------------------------

--
-- Table structure for table `product_type`
--

CREATE TABLE `product_type` (
  `ID` int(11) NOT NULL,
  `productType` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `product_type`
--

INSERT INTO `product_type` (`ID`, `productType`) VALUES
(1, 'Other'),
(2, 'General & Home Use'),
(3, 'Ready To Eat'),
(4, 'Bakery & Industry Use');

-- --------------------------------------------------------

--
-- Table structure for table `purchase_categories`
--

CREATE TABLE `purchase_categories` (
  `ID` int(11) NOT NULL,
  `categoryId` int(11) NOT NULL,
  `purchaseId` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `purchase_categories`
--

INSERT INTO `purchase_categories` (`ID`, `categoryId`, `purchaseId`) VALUES
(5, 7, 3),
(6, 8, 3),
(7, 7, 4),
(8, 6, 5),
(9, 9, 6),
(10, 13, 7),
(11, 14, 8),
(13, 1, 10),
(14, 1, 9),
(137, 13, 118),
(139, 13, 117),
(140, 13, 116),
(141, 13, 115),
(142, 14, 114),
(143, 14, 113),
(145, 14, 111),
(147, 14, 109),
(148, 14, 108),
(149, 14, 107),
(150, 14, 106),
(151, 14, 105),
(152, 14, 104),
(153, 14, 103),
(154, 14, 102),
(155, 14, 101),
(156, 14, 100),
(157, 14, 99),
(158, 14, 98),
(159, 14, 97),
(160, 14, 96),
(161, 1, 95),
(162, 1, 94),
(163, 1, 92),
(164, 1, 93),
(165, 1, 91),
(166, 1, 90),
(167, 1, 89),
(168, 1, 88),
(169, 1, 87),
(170, 12, 86),
(171, 12, 85),
(172, 12, 84),
(173, 12, 83),
(174, 12, 82),
(175, 8, 80),
(176, 8, 81),
(177, 8, 79),
(178, 8, 78),
(179, 8, 77),
(180, 8, 76),
(181, 8, 75),
(182, 8, 74),
(183, 8, 73),
(184, 8, 72),
(185, 8, 71),
(186, 8, 70),
(187, 8, 69),
(188, 8, 68),
(189, 8, 67),
(190, 8, 66),
(191, 10, 65),
(192, 10, 64),
(193, 10, 63),
(194, 10, 62),
(195, 10, 61),
(196, 2, 60),
(197, 2, 59),
(198, 2, 58),
(199, 2, 57),
(200, 2, 56),
(201, 2, 55),
(202, 2, 54),
(203, 2, 53),
(204, 2, 52),
(205, 2, 51),
(206, 2, 50),
(207, 2, 49),
(208, 2, 48),
(209, 2, 47),
(210, 2, 46),
(211, 2, 45),
(212, 6, 44),
(213, 6, 43),
(214, 6, 42),
(215, 6, 41),
(216, 6, 40),
(217, 6, 39),
(218, 6, 38),
(219, 6, 37),
(220, 6, 36),
(221, 6, 35),
(222, 6, 34),
(223, 6, 33),
(224, 6, 32),
(225, 6, 31),
(226, 6, 30),
(227, 6, 29),
(228, 6, 28),
(229, 11, 26),
(230, 11, 27),
(231, 11, 25),
(232, 11, 24),
(233, 11, 23),
(234, 11, 22),
(235, 11, 21),
(236, 11, 20),
(237, 11, 19),
(238, 7, 18),
(239, 7, 17),
(240, 7, 16),
(241, 7, 15),
(242, 7, 14),
(243, 7, 13),
(244, 7, 12),
(245, 1, 11),
(249, 13, 119),
(251, 14, 112),
(252, 14, 110);

-- --------------------------------------------------------

--
-- Table structure for table `purchase_master`
--

CREATE TABLE `purchase_master` (
  `ID` int(11) NOT NULL,
  `customerId` int(20) NOT NULL,
  `shippingId` int(20) NOT NULL,
  `purchase_date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `purchase_type`
--

CREATE TABLE `purchase_type` (
  `ID` int(11) NOT NULL,
  `typeId` int(11) NOT NULL,
  `purchaseId` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `purchase_type`
--

INSERT INTO `purchase_type` (`ID`, `typeId`, `purchaseId`) VALUES
(1, 3, 3),
(2, 4, 4),
(3, 4, 5),
(4, 2, 6),
(5, 4, 7),
(6, 4, 8),
(7, 2, 9),
(8, 4, 10),
(423, 4, 11),
(422, 3, 11),
(421, 4, 12),
(420, 2, 12),
(419, 4, 13),
(418, 4, 14),
(417, 4, 15),
(416, 4, 16),
(415, 4, 17),
(414, 4, 18),
(413, 4, 19),
(412, 3, 19),
(411, 2, 19),
(410, 4, 20),
(409, 2, 20),
(408, 4, 21),
(407, 2, 21),
(406, 4, 22),
(405, 3, 23),
(404, 4, 24),
(403, 3, 24),
(402, 2, 24),
(401, 4, 25),
(400, 2, 25),
(397, 4, 26),
(396, 2, 26),
(399, 4, 27),
(398, 2, 27),
(395, 4, 28),
(394, 3, 28),
(393, 2, 28),
(392, 4, 29),
(391, 3, 29),
(390, 2, 29),
(389, 4, 30),
(388, 3, 30),
(387, 2, 30),
(386, 4, 31),
(385, 3, 31),
(384, 2, 31),
(383, 4, 32),
(382, 3, 32),
(381, 2, 32),
(380, 4, 33),
(379, 3, 33),
(378, 2, 33),
(377, 4, 34),
(376, 3, 34),
(375, 2, 34),
(374, 4, 35),
(373, 3, 35),
(372, 2, 35),
(371, 4, 36),
(370, 2, 36),
(369, 4, 37),
(368, 3, 37),
(367, 2, 37),
(366, 4, 38),
(365, 2, 38),
(364, 4, 39),
(363, 4, 40),
(362, 3, 40),
(361, 2, 40),
(360, 4, 41),
(359, 4, 42),
(358, 2, 42),
(357, 4, 43),
(356, 2, 43),
(355, 4, 44),
(354, 2, 44),
(353, 4, 45),
(352, 4, 46),
(351, 4, 47),
(350, 4, 48),
(349, 4, 49),
(348, 4, 50),
(347, 4, 51),
(346, 2, 51),
(345, 4, 52),
(344, 4, 53),
(343, 4, 54),
(342, 4, 55),
(341, 2, 55),
(340, 4, 56),
(339, 4, 57),
(338, 4, 58),
(337, 4, 59),
(336, 4, 60),
(335, 4, 61),
(334, 2, 61),
(333, 4, 62),
(332, 2, 62),
(331, 4, 63),
(330, 2, 63),
(329, 4, 64),
(328, 2, 64),
(327, 4, 65),
(326, 2, 65),
(325, 4, 66),
(324, 2, 66),
(323, 4, 67),
(322, 4, 68),
(321, 2, 68),
(320, 4, 69),
(319, 2, 69),
(318, 4, 70),
(317, 2, 70),
(316, 4, 71),
(315, 2, 71),
(314, 4, 72),
(313, 2, 72),
(312, 4, 73),
(311, 2, 73),
(310, 4, 74),
(309, 2, 74),
(308, 4, 75),
(307, 2, 75),
(306, 4, 76),
(305, 2, 76),
(304, 4, 77),
(303, 2, 77),
(302, 4, 78),
(301, 2, 78),
(300, 1, 78),
(299, 4, 79),
(298, 2, 79),
(294, 4, 80),
(293, 2, 80),
(297, 4, 81),
(296, 3, 81),
(295, 2, 81),
(292, 4, 82),
(291, 2, 82),
(290, 4, 83),
(289, 2, 83),
(288, 4, 84),
(287, 2, 84),
(286, 4, 85),
(285, 2, 85),
(284, 4, 86),
(283, 2, 86),
(282, 4, 87),
(281, 2, 87),
(280, 4, 88),
(279, 2, 88),
(278, 4, 89),
(277, 4, 90),
(276, 4, 91),
(274, 4, 92),
(275, 4, 93),
(273, 4, 94),
(272, 2, 94),
(271, 4, 95),
(270, 4, 96),
(269, 4, 97),
(268, 4, 98),
(267, 4, 99),
(266, 4, 100),
(265, 4, 101),
(264, 4, 102),
(263, 1, 102),
(262, 4, 103),
(261, 1, 103),
(260, 4, 104),
(259, 1, 104),
(258, 4, 105),
(257, 2, 105),
(256, 1, 105),
(255, 4, 106),
(254, 1, 106),
(253, 4, 107),
(252, 2, 107),
(251, 1, 107),
(250, 4, 108),
(249, 4, 109),
(434, 4, 110),
(247, 4, 111),
(246, 2, 111),
(433, 1, 112),
(244, 4, 113),
(243, 2, 113),
(242, 1, 113),
(241, 4, 114),
(240, 1, 114),
(239, 4, 115),
(238, 1, 115),
(237, 4, 116),
(236, 1, 116),
(235, 4, 117),
(234, 2, 117),
(233, 1, 117),
(229, 4, 118),
(228, 2, 118),
(227, 1, 118),
(431, 4, 119),
(430, 1, 119);

-- --------------------------------------------------------

--
-- Table structure for table `shipping_details`
--

CREATE TABLE `shipping_details` (
  `ID` int(11) NOT NULL,
  `customerId` int(11) NOT NULL,
  `houseName` varchar(20) NOT NULL,
  `houseNo` int(11) NOT NULL,
  `street` varchar(20) NOT NULL,
  `city` varchar(20) NOT NULL,
  `district` varchar(20) NOT NULL,
  `state` varchar(20) NOT NULL,
  `country` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `table_cart`
--

CREATE TABLE `table_cart` (
  `ID` int(11) NOT NULL,
  `customerId` int(11) NOT NULL,
  `productId` int(11) NOT NULL,
  `quantity` int(11) NOT NULL,
  `amount` int(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `table_purchase`
--

CREATE TABLE `table_purchase` (
  `ID` int(11) NOT NULL,
  `masterId` int(11) NOT NULL,
  `productId` int(11) NOT NULL,
  `quantity` int(11) NOT NULL,
  `delivery_date` date NOT NULL,
  `amount` int(11) NOT NULL,
  `status` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `banner_image`
--
ALTER TABLE `banner_image`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `customer`
--
ALTER TABLE `customer`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `inner_banner_image`
--
ALTER TABLE `inner_banner_image`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `login`
--
ALTER TABLE `login`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `product_creation`
--
ALTER TABLE `product_creation`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `product_pic`
--
ALTER TABLE `product_pic`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `product_purchase`
--
ALTER TABLE `product_purchase`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `product_type`
--
ALTER TABLE `product_type`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `purchase_categories`
--
ALTER TABLE `purchase_categories`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `purchase_master`
--
ALTER TABLE `purchase_master`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `purchase_type`
--
ALTER TABLE `purchase_type`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `shipping_details`
--
ALTER TABLE `shipping_details`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `table_cart`
--
ALTER TABLE `table_cart`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `table_purchase`
--
ALTER TABLE `table_purchase`
  ADD PRIMARY KEY (`ID`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `banner_image`
--
ALTER TABLE `banner_image`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;
--
-- AUTO_INCREMENT for table `customer`
--
ALTER TABLE `customer`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `inner_banner_image`
--
ALTER TABLE `inner_banner_image`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `login`
--
ALTER TABLE `login`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=35;
--
-- AUTO_INCREMENT for table `product_creation`
--
ALTER TABLE `product_creation`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=122;
--
-- AUTO_INCREMENT for table `product_pic`
--
ALTER TABLE `product_pic`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=266;
--
-- AUTO_INCREMENT for table `product_purchase`
--
ALTER TABLE `product_purchase`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=120;
--
-- AUTO_INCREMENT for table `product_type`
--
ALTER TABLE `product_type`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `purchase_categories`
--
ALTER TABLE `purchase_categories`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=253;
--
-- AUTO_INCREMENT for table `purchase_master`
--
ALTER TABLE `purchase_master`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `purchase_type`
--
ALTER TABLE `purchase_type`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=435;
--
-- AUTO_INCREMENT for table `shipping_details`
--
ALTER TABLE `shipping_details`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `table_cart`
--
ALTER TABLE `table_cart`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `table_purchase`
--
ALTER TABLE `table_purchase`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
