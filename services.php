<?php require("header.php"); ?>
    <div class="cat_banr_part contact_pg" xmlns="http://www.w3.org/1999/html">
        <div class="container">
            <div class="row">
                <div class="col-lg-3 col-md-3">
                    <h4>CATEGORIES<span class="cat_trigger"><i class="fa fa-plus"></i></span></h4>

                    <?php

                    $selcatQuery = "SELECT * FROM ".TABLE_CATEGORIES."";
                    $selectcatAll= $db->query($selcatQuery);

                    ?><ul class="categories"><?php
                        while($catRows = mysql_fetch_array($selectcatAll))
                        {
                            ?>
                            <li><a href="products.php?cat=<?php echo $catRows['ID'] ?>"><?php echo $catRows['categoryName'] ?></a></li>



                        <?php } ?>
                    </ul>
                </div>
                <div class="col-lg-9 col-md-9">
                    <div class="about_pg_part">
                        <h4>Services</h4>
                        <div class="service_wrap">
                            <div class="row">
                                <div class="col-lg-12 col-md-4 col-sm-4">
                                    <div class="service_box">
                                       <!-- <div class="service_img">
                                            <img src="images/service.jpg" class="img-responsive" alt="" />
                                        </div>-->
                                        <h3>Distribution Network and Sales Counter</h3>
                                        <p>For catering the requirements of all classes of customers with utmost care and quality,

we have distribution network in Calicut, Kannur, Wayanand and Malappuram for delivering the

products at the Customers spots and factories and fully equipped Sales Counters targeting

walking customers.</p>
                                    </div>
                                </div>
                                <div class="col-lg-12 col-md-4 col-sm-4">
                                    <div class="service_box">
                                       <!-- <div class="service_img">
                                            <img src="images/service.jpg" class="img-responsive" alt="" />
                                        </div>-->
                                        <h3>Consultancy Service</h3>
                                        <p>Having experience of more than Ten years in the Baking field, we provide consultancy

services for A to Z requirements of this segment. We provide the service of professional experts

for opening a new Sales Counter, Setting up of factory, installation of new equipments etc.</p>
                                    </div>
                                </div>
                                <div class="col-lg-12 col-md-4 col-sm-4">
                                    <div class="service_box">
                                       <!-- <div class="service_img">
                                            <img src="images/service.jpg" class="img-responsive" alt="" />
                                        </div>-->
                                        <h3>Training and Demonstrations</h3>
                                        <p>Being a fast growing segment; customs, taste and technology implementation is

changing very quickly. To survive and for growing, updating is essential. We provide awareness

about latest trends, taste, productions and implementation of technology through various

training programs and demonstrations.</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php require("footer1.php"); ?>
<?php require("footer2.php"); ?>