<?php require("header.php"); ?>
    <div class="cat_banr_part">
        <div class="container">
            <div class="row">
               <div class="col-lg-3 col-md-3">
                    <h4>CATEGORIES<span class="cat_trigger"><i class="fa fa-plus"></i></span></h4>
                    
                    <?php
       
       					$selcatQuery = "SELECT * FROM ".TABLE_CATEGORIES."";
						$selectcatAll= $db->query($selcatQuery);
				     ?>
						<ul class="categories">
						<?php
							while($catRows = mysql_fetch_array($selectcatAll))
								{
					  ?>
								<li><a href="products.php?cat=<?php echo $catRows['ID'] ?>"><?php echo $catRows['categoryName'] ?></a></li>
					       <?php } ?>
								
						</ul>
                </div>
                <div class="col-lg-5 col-md-5 col-sm-6">
                    <div class="contact_pg_part">
                        <h2>Contact us</h2>
                        <form method="post" action="contactmail.php">
                            <input type="text" placeholder="Name" name="name" required="">
                            <input type="email" placeholder="Email" name="email" >
                            <input type="text" placeholder="Phone" name="phone">
                            <textarea placeholder="Measssage..." name="message" required=""></textarea>
                            <button type="submit">SEND</button>
                        </form>
                    </div>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-4">
                    <div class="contact_pg_adrs_part">
                       <div class="panel panel-primary">
                           <div class="panel-heading">Corporate Office</div>
                           <div class="panel-body">
                               <p>RB Trade India, Rolex Arcade<br />Parappanpoil PO, Thamarassery VIA<br />Calicut, Kerala-673573
                                   <br /> Email : <a href="mailto:rbtradeindia@gmail.com">rbtradeindia@gmail.com</a></p>
                           </div>
                       </div>

                        <div class="panel panel-primary">
                            <div class="panel-heading">Sister Concerns</div>
                            <div class="panel-body">
                                <p><strong>FLAVOR MART</strong><br />PPR Complex, Nr.Edakkad Village Office
                                    <br> Kizhunna PO, Thottada
                                    <br> Kannur - 670007, Kerala, India
                                    <br> Mob : <a href="tel:919349303767">+91 9349303767</a>
                                    <br> Email : <a href="mailto:flavormart@gmail.com">flavormart@gmail.com</a></p>
                                <p><strong>FLAVOR MART</strong><br />SR Towers<br />Bye Pass Road, Kalpetta</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div> 
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12">
                    <div class="map_part">
                        <h2>Locate us</h2>
                    </div>
                    <div class="map">
                        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d6567.22410364845!2d75.41798622903163!3d11.842636030738477!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3ba4236f1c47def3%3A0x8528c4126a664e6b!2sThottada%2C+Kerala+670007!5e0!3m2!1sen!2sin!4v1461581270784" width="100%" height="100%" frameborder="0" style="border:0" allowfullscreen></iframe>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php require("footer1.php"); ?>
<?php require("footer2.php"); ?>    