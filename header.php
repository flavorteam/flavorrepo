<?php

require("flavoradmin/config/config.inc.php"); 
require("flavoradmin/config/Database.class.php");
require("flavoradmin/config/Application.class.php");

$db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE); 
$db->connect();
?>

<!DOCTYPE HTML>
<html lang="">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Flavor Mart</title>
    <link rel="icon" href="images/x_icn.png">
    <link rel="stylesheet" type="text/css" href="css/bootstrap.css">
    <link rel="stylesheet" type="text/css" href="css/jquery.bxslider.css">
    <link rel="stylesheet" type="text/css" href="font_awesome/css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="css/animate.css">
    <link href="https://fonts.googleapis.com/css?family=PT+Sans:400,700" rel="stylesheet" type="text/css">
    <link rel="stylesheet" type="text/css" href="css/flexisel_style.css">
    <link rel="stylesheet" type="text/css" href="light/lightgallery.css">
    <link rel="stylesheet" type="text/css" href="css/style.css">
    <script src="js/jquery-2.1.4.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/jquery.bxslider.js"></script>
    <script src="js/jquery.flexisel.js"></script>
    <script src="js/jquery.elevatezoom.js"></script>
    <script src="js/script.js"></script>
</head>

<body>
<?php 
if (@isset($_SESSION['msg'])) {
echo $_SESSION['msg'];
}
unset($_SESSION['msg']); 
?>
    <header>
        <div class="container">
            <div class="track_nav">
                <ul>
                    <li><a href="terms_and_conditions.php">Terms and Conditions</a>|</li>
                    <li><a href="recipie.php">Recipes</a>|</li>
                    
                    <?php if(isset($_SESSION['loginId']))
                    {
                    ?>
						<li>Hi<span style="font-weight: bold">  <?php echo @$_SESSION['username']; ?></span> !</li>
						<li><a href="logout.php">Sign out</a></li>
				<?php }else{ ?>
                    <li><a href="#" data-toggle="modal" data-target="#user_modal">Sign in</a></li>
                    <?php } ?>
                </ul>
            </div>
            <div class="main_warp">
                <div class="logo">
                    <img src="images/log.png" alt="">
                </div>
                
                <div class="main_nav">
                	<div class="search_tab">  
	                    <?php 
	                    if(isset($_SESSION['loginId'])){
	                    $loginId = $_SESSION['loginId'];
	                    
	                    $qry = "SELECT count(customerId) as total FROM ".TABLE_CART." WHERE customerId=$loginId "; 
	                    $qry2 = mysql_query($qry);
	                    $qryResult = mysql_fetch_array($qry2);
	               		$resultValue = $qryResult['total'];
	                    ?>
	                    <a href="cart.php" id="cart">
	                    <img src="images/cart.png" alt="" />
	                    <span><?= $resultValue; ?></span></a>
	                    <?php } else { ?>
							<a href="cart.php" id="cart">
	                    <img src="images/cart.png" alt=""/>
	                    <span></span></a>
						<?php } ?>
	                </div>
                    <ul>
                        <li><a href="index.php">Home</a></li>
                        <li><a href="about_us.php">Our Company</a></li>
                        <li><a href="products.php">Products</a></li>
                        <li><a href="services.php">Services</a></li>
                        <li><a href="gallery.php">Gallery</a></li>
                        <!--<li><a href="facilities.php">OUR FACILITES</a></li>
                        <li><a href="subdealers.php">OUR SUBDEALERS</a></li>
                        <li><a href="mode_of.php">MODE OF OPERATION</a></li>-->
                        <li><a href="contact_us.php">CONTACT US</a></li>
                    </ul>
                </div>
            </div>
        </div>       
    </header>
 