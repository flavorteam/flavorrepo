<?php require("header.php"); ?>
    <div class="cat_banr_part">
        <div class="container">
            <div class="row">
                <div class="col-lg-3 col-md-3">
                    <h4>CATEGORIES<span class="cat_trigger"><i class="fa fa-plus"></i></span></h4>
                    <?php
       
       					$selcatQuery = "SELECT * FROM ".TABLE_CATEGORIES."";
						$selectcatAll= $db->query($selcatQuery);
						
						?><ul class="categories"><?php
							while($catRows = mysql_fetch_array($selectcatAll))
								{
								?>
								<li><a href="products.php?cat=<?php echo $catRows['ID'] ?>"><?php echo $catRows['categoryName'] ?></a></li>
								
								
								
						<?php } ?>
						</ul>
                </div>
                <div class="col-lg-9 col-md-9">
                    <div class="mode_pg_part">
                        <h2>Terms and Conditions</h2>
                        <hr style="border-top:1px solid #000; margin-top:5px;">
                        <div class="mod_opt">
                            <ul>
                                <li>The content of the pages of this website is for your general information and use only. It is subject to change without notice.</li>
                                <li>This website uses cookies to monitor browsing preferences. If you do allow cookies to be used, the following personal information may be stored by us for use by third parties: [insert list of information].</li>
                                <li>Neither we nor any third parties provide any warranty or guarantee as to the accuracy, timeliness, performance, completeness or suitability of the information and materials found or offered on this website for any particular purpose. You acknowledge that such information and materials may contain inaccuracies or errors and we expressly exclude liability for any such inaccuracies or errors to the fullest extent permitted by law.</li>
                                <li>Your use of any information or materials on this website is entirely at your own risk, for which we shall not be liable. It shall be your own responsibility to ensure that any products, services or information available through this website meet your specific requirements.</li>
                                <li>This website contains material which is owned by or licensed to us. This material includes, but is not limited to, the design, layout, look, appearance and graphics. Reproduction is prohibited other than in accordance with the copyright notice, which forms part of these terms and conditions.</li>
                                <li>All trade marks reproduced in this website which are not the property of, or licensed to, the operator are acknowledged on the website.</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php require("footer2.php"); ?>
<?php require("footer1.php"); ?>