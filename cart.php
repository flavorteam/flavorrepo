<?php
require('header.php');
?>
<div class="cat_banr_part contact_pg">
    <div class="container">
        <div class="row">
            <div class="col-lg-3 col-md-3 col-sm-3">
                <h4>CATEGORIES</span></h4>
                <?php
       
       					$selcatQuery = "SELECT ID, categoryName FROM ".TABLE_CATEGORIES."";
						$selectcatAll= $db->query($selcatQuery);
						
						?><ul class="categories"><?php
							while($catRows = mysql_fetch_array($selectcatAll))
								{
								?>
								<li><a href="products.php?cat=<?php echo $catRows['ID'] ?>"><?php echo $catRows['categoryName'] ?></a></li>		
								
						<?php } ?>
                </ul>
            </div>
            <div class="col-lg-9 col-md-9 col-sm-9">
                <div class="cart_wrap">
                    <h4 class="page_head">Shopping Cart</h4>
                    <div class="row">
                        <div class="col-lg-9 col-md-9 col-sm-9">
                        
                        <?php 
                        $loginId = @$_SESSION['loginId'];
                        $qry = "SELECT ".TABLE_CART.".ID,
                        			   ".TABLE_CART.".customerId,
                        			   ".TABLE_CART.".productId,
                        			   ".TABLE_CART.".quantity,
                        			   ".TABLE_PRODUCT_CREATION.".productName,
                        			   ".TABLE_PRODUCT_PURCHASE.".flavormartRate,
                        			   ".TABLE_PRODUCT_PIC.".picture 
                        		FROM   ".TABLE_CART.",".TABLE_PRODUCT_PURCHASE.",".TABLE_PRODUCT_CREATION.",".TABLE_PRODUCT_PIC." 
                        		WHERE ".TABLE_CART.".customerId='$loginId'
                        		AND ".TABLE_CART.".productId=".TABLE_PRODUCT_PURCHASE.".ID
                        		AND ".TABLE_PRODUCT_PURCHASE.".productCreateId =".TABLE_PRODUCT_CREATION.".ID 
                        		AND ".TABLE_PRODUCT_PIC.".productId = ".TABLE_PRODUCT_PURCHASE.".ID GROUP BY ".TABLE_CART.".ID";
                        		//echo $qry;
                        $qry2 = mysql_query($qry);
                        $no = mysql_num_rows($qry2);
                        //echo $no; 
                        if($no>0)
                        {
						$sum = 0;
						
                        while($row = mysql_fetch_array($qry2))
                        {
							$total	=	$row['quantity']*$row['flavormartRate'];	
							$sum	=	$sum+$total;
                         ?>
                         <input  type="hidden" name="sum" class="sum" value="<?php echo $sum;?>" />
                            <div class="cart_item">
                                <div class="cart_img">
                                    <img src="flavoradmin/cPanel/product_purchase/<?php echo $row['picture']; ?>" alt="" />
                                </div>
                                
                                <div class="cart_det">
                                    <h4 class="cart_title"><?= $row['productName']; ?></h4>
                                    <p class="cart_price"><i class="fa fa-rupee"></i><span><?= $row['flavormartRate']; ?></span></p>
                                   
                                    <form method="post" action="do.php?op=edit">
                                    	<div class="form_block">
                                    		<input type="hidden" name="id" value="<?= $row['ID']; ?>"/>
	                                        <input name="quantity" type="number" min="1" value="<?= $row['quantity']; ?>">
	                                        <button type="submit">Update</button>
	                                    </div>
                                    </form>                                    
                                    <a href="do.php?op=delete&id=<?= $row['ID']; ?>" class="remove_cart">Remove <i class="fa fa-times"></i></a>
                                </div>
                                <div style="clear: both;"></div>
                            </div>
                       	<?php }	
						 ?>
                        </div>
                        <div class="col-lg-3 col-md-3 col-sm-3">
                            <div class="proceed">
                                <button type="button" data-toggle="modal" data-target="#checkout_form">Proceed to Checkout</button>
                            </div>
                        </div> 
                        
                        <!-------Grant total div------>
                        <div style="clear:both;"></div>
	                    <div class="grant_total">
	                       <strong style="color:#0e594e;">Grant Total:</strong> &nbsp;<span id="granttotal"></span>
	                    </div>  
	                                          
                        <?php }else{?>
                        <p style="margin-top: 15px">Your Shopping Cart is empty. </p>
                        <?php }?>
                    </div>
                   
                </div>
            </div>
        </div>
    </div>
</div>
<?php
require('footer1.php');
?>
<script>
	$(document).ready(function () {
		var sum = $('.cart_wrap .sum:last').val();
		$('#granttotal').html('<i class="fa fa-rupee"></i>' + sum);
	});
</script>
<?php
require('footer2.php');
?>
