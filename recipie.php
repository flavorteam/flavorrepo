<?php require("header.php"); ?>
    <div class="cat_banr_part contact_pg">
        <div class="container">
            <div class="row">
                <div class="col-lg-3 col-md-3">
                    <h4>CATEGORIES<span class="cat_trigger"><i class="fa fa-plus"></i></span></h4>
                    
                    <?php
       
       					$selcatQuery = "SELECT * FROM ".TABLE_CATEGORIES."";
						$selectcatAll= $db->query($selcatQuery);
						
						?><ul class="categories"><?php
							while($catRows = mysql_fetch_array($selectcatAll))
								{
								?>
								<li><a href="products.php?cat=<?php echo $catRows['ID'] ?>"><?php echo $catRows['categoryName'] ?></a></li>
								
								
								
						<?php } ?>
						</ul>
                </div>
                <div class="col-lg-9 col-md-9">
                    <div class="about_pg_part">
                        <h2>Recipies</h2>
                    </div>
					<div class="recipie_block">
						<div class="recipie_img">
							<img src="images/recipie1.jpg" alt="" />
						</div>
						<div class="pizza_det">
							<h4>How to make Pizza at home</h4>
							<ol class="steps">
								<li>Preheat oven at 220 C for 30 mts.
</li>
								<li>In a large wide bowl, add the maida/flour, salt, dried herbs, sugar or honey, yeast and olive oil and mix to combine. Slowly add the warm water and make a dough. If the dough is dry, add a little more water and knead gently for 5-6 mts into a smooth firm dough. Grease the bowl with olive oil and place the dough in the bowl, cover with cling film and set aside in a warm place for 15 mts.
</li>
								<li>Turn the dough onto a lightly floured surface and divide into 2 equal portions. Cover them with plastic wrap and allow to rest for another 5 mts. This step helps you to roll out a round shape with the right amount of elasticity for rolling.
</li>
								<li>Lightly grease the back side of cookie sheet (place a cookie sheet upside down) with oil and sprinkle cornmeal or semolina liberally (this will give a good crunch to the base as well ease the removal of the pizza from the cookie sheet without sticking)
</li>
								<li>Place a ball of dough on the cookie sheet and spread it out into a flat disk using your hands. After flattening it to a circle, roll the base as thin as possible using your rolling pin.  Ensure that the base is evenly spread out and the edges are slightly thick as shown in the image above.
</li>
								<li>Brush the rim of the pizza base with olive oil, spread 2 heaped tbsps of the pizza sauce evenly all over. Sprinkle the grated mozarella cheese liberally all over.
</li>
								<li>Add any toppings of your choice like tomato slices or bell pepper juliennes, caramelized onions, grilled paneer or chicken and fresh basil leaves. The choice is endless. Sprinkle Parmesan cheese if using.
</li>
								<li>Slide the cookie sheet onto the lower rack of your oven and bake for 12-15 minutes, or until the cheese turns golden brown and the pizza base browns along the edges. Allow to rest for 3-4 minutes before slicing. (check from 12 mts on wards - oven heat varies from oven to oven)
</li>
								<li>To prepare Pizza Sauce: In a heavy bottomed vessel, add olive oil and heat on medium flame. Add the minced garlic and saute for a mt. Add the dried oregano and dried basil and mix. Add onions and saute for 5 mts. Add the chopped tomatoes and saute for 3 mts on high. Reduce flame and simmer for 20 mts without lid.
</li>
								<li>Add tomato puree, pepper powder, red chili flakes and salt and mix. Continue to simmer till it becomes a sauce like consistency.
</li>
								<li>Turn off flame and cool. You can blend to a coarse paste at this stage or use the cooked sauce right away. If using fresh oregano or basil, add it towards the end of the cooking process.</li>
						    </ol>
						</div>
					</div>
                </div>
            </div>    
        </div>
    </div>
<?php require("footer1.php"); ?>
<?php require("footer2.php"); ?>