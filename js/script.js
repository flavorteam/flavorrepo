/*jslint browser: true*/
/*global $, jQuery, alert*/
$(function(){
    'use strict';
	$('.cat_trigger').click(function() {
        var curTrigger = $(this),
            targetDiv = $('.categories'),
            flag = 0;
        targetDiv.slideToggle(300, function () {
            if (targetDiv.is(':hidden')) {
                curTrigger.find('.fa').removeClass('fa-minus').addClass('fa-plus');
            } else {
                curTrigger.find('.fa').removeClass('fa-plus').addClass('fa-minus');
            }    
        });
    });
    $('.banner').bxSlider({
        auto: true,
        nextSelector: '#banner_next',
        prevSelector: '#banner_prev',
        nextText: '<i class="fa fa-angle-right"></i>',
        prevText: '<i class="fa fa-angle-left"></i>',
        onSliderLoad: function () {
            $('.banner>li .cap_block').eq(1).addClass('active-slide');
            $(".cap_block.active-slide").addClass("wow animated fadeInUp");
        },
        onSlideAfter: function (currentSlideNumber, totalSlideQty, currentSlideHtmlObject) {
            //console.log(currentSlideHtmlObject);
            $('.active-slide').removeClass('active-slide');
            $('.banner>li .cap_block').eq(currentSlideHtmlObject + 1).addClass('active-slide');
            $(".cap_block.active-slide").addClass("wow animated fadeInUp");

        },
        onSlideBefore: function () {
            $(".cap_block.active-slide").removeClass("wow animated fadeInUp");
            $(".one.cap_block.active-slide").removeAttr('style');
        }
    });
    $('.testimonial_slider').bxSlider({
        auto: true,
        speed: 1000,
        easing: 'ease-out'
    });
    // toggling category combo box
    $(document).on('click', '.filter_cat_btn[data-action="toggle_cat"]', function (e) {
        e.preventDefault();
        $(this).siblings('.category_combo_list').slideToggle();
    });
    $(document).click(function (e) {
        var clickedEl = e.target;
        if ($(clickedEl).hasClass('filter_cat_btn') || $(clickedEl).hasClass('category_combo_list') || $(clickedEl).parents('.category_combo_list').length > 0) {
            var a = 'jkjl';
        } else {
            $('.category_combo_list').hide();
        }
    });

    // Toggling between register and login
    $(document).on('click', '.user_controls a', function (e) {
        e.preventDefault();
        var a = $(this),
            target = $(a.attr('href'));
        $('.user_controls a').not(a).removeClass('active');
        a.addClass('active');
        $('.user_block').not(target).removeClass('active');
        target.addClass('active');
    });

    // Load more action
	$(document).on('click', '.load_more_product a', function(){
        //e.preventDefault();
        var curLoadMore = $(this),
            qry = $.trim(curLoadMore.data('qry')),
            limit = $.trim(curLoadMore.data('limit')),
            targetDiv = curLoadMore.parent().prev();

        //loadMore(qry, limit);
        //console.log(qry);
        $.ajax({
            url: 'loadMoreProduct.php?start='+limit+'&qry='+qry,
            type: 'GET',
            success: function(result){
                //console.log(result);
                targetDiv.after(result);
                curLoadMore.parent().remove();
            },
            error: function(error){
                console.log(error);
            }
        });
    });
	
	// Product image zooming
	$("#zoom_03").elevateZoom({
        gallery: 'gallery_01',
        cursor: 'pointer',
        galleryActiveClass: "active",
        imageCrossfade: true,
        responsive: true,
        zoomType:   'inner',
        loadingIcon: "http://www.elevateweb.co.uk/spinner.gif"
    });

    $("#zoom_03").bind("click", function (e) {
        var ez = $('#zoom_03').data('elevateZoom');
        ez.closeAll(); //NEW: This function force hides the lens, tint and window
        $.fancybox(ez.getGalleryList());
        return false;
    });
});
// Trigger session message
$(window).load(function () {
    'use strict';
    // Session Message
    $('#session_modal').modal('show');
});
jQuery(document).ready(function($){
    //you can now use $ as your jQuery object.
    if ($('.gallery_wrap').length > 0) {
        $('.gallery_wrap').lightGallery({
            download: false,
            thumbnail: false,
            fullScreen: false,
            selector: '.row .col-lg-4 a'
        });
    }
});














