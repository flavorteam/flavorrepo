<?php require("header.php"); ?>
    <div class="cat_banr_part contact_pg" xmlns="http://www.w3.org/1999/html">
        <div class="container">
            <div class="row">
                <div class="col-lg-3 col-md-3">
                    <h4>CATEGORIES<span class="cat_trigger"><i class="fa fa-plus"></i></span></h4>
                    
                    <?php
       
       					$selcatQuery = "SELECT * FROM ".TABLE_CATEGORIES."";
						$selectcatAll= $db->query($selcatQuery);
						
						?><ul class="categories"><?php
							while($catRows = mysql_fetch_array($selectcatAll))
								{
								?>
								<li><a href="products.php?cat=<?php echo $catRows['ID'] ?>"><?php echo $catRows['categoryName'] ?></a></li>
								
								
								
						<?php } ?>
						</ul>
                </div>
                <div class="col-lg-9 col-md-9">
                    <div class="about_pg_part">
                        <h4>About us</h4>
                        <p class="about">Flavor Mart is an initiative for providing high quality and hygienic food products and raw materials for

Hotels, Bakery and Catering Units (HBC). A Calicut based stockiest and supplier of all range of food

products, we cater the needs of Bakers and Hotels of North Malabar region at present.

In the voyage along with innovations and solutions, we ensure better service at our level best. This is our

Trade Secret.</p>
                        <p style="padding:0;">Our Motto is<b> “QUALITY, TRUST, GROWTH”</b></p>
                        <!--<h4>Our History</h4>
                        <p>FLAVOR MART traces its roots back to 2011 when it was started in Parappan Poyil, the rural part of Kozhikode district two by aiming to be a provider and a poineer for food industries in our surroundings.<br /><br />Today FLAVOR MART is one of the major distributor in Malabar with different types of products and food ingredients for hotels, bakeries and other food industries. Also helps to move products and ingredients from different parts of the world to consumers through the shelves of hypermarkets, supermarkets, small retail shops etc... FLAVOR MART had given importance to relationship that of business and believe that business and reaction can be thrive together, since we made good customer network.</p> -->
                        <h4>Our Mission</h4>
                        <p>Provide the complete solution for all your requirements of HBC at your door step.<!--Our mission is to serve, provide, teach and taste varieties of latest trends in menu at earliest in high quality. Our vision is customer satisfaction and easiness, provide all types of food and related items under one roof to obtain at any time, for local customers we provided a working counter in our complex and for others there is a van service and online shopping also.--></p>
                        <!--<h4>Management Team</h4>
                        <div class="team_wrap">
                            <div class="row">
                                <div class="col-lg-3 col-md-3 col-sm-3">
                                    <div class="team_box">
                                        <div class="team_img">
                                            <img src="images/team_img.jpg" alt="">
                                        </div>
                                        <h5>John Doe</h5>
                                        <p>CEO</p>
                                    </div>
                                </div>
                                <div class="col-lg-3 col-md-3 col-sm-3">
                                    <div class="team_box">
                                        <div class="team_img">
                                            <img src="images/team_img.jpg" alt="">
                                        </div>
                                        <h5>John Doe</h5>
                                        <p>CEO</p>
                                    </div>
                                </div>
                                <div class="col-lg-3 col-md-3 col-sm-3">
                                    <div class="team_box">
                                        <div class="team_img">
                                            <img src="images/team_img.jpg" alt="">
                                        </div>
                                        <h5>John Doe</h5>
                                        <p>CEO</p>
                                    </div>
                                </div>
                                <div class="col-lg-3 col-md-3 col-sm-3">
                                    <div class="team_box">
                                        <div class="team_img">
                                            <img src="images/team_img.jpg" alt="">
                                        </div>
                                        <h5>John Doe</h5>
                                        <p>CEO</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <br />--><br />
                        <h4>Our Milestones</h4>
                        <table class="mile_stones table table-bordered">
                            <tbody>
                                <tr>
                                    <td>2009</td>
                                    <td>Established in 2009 as “Neha Agencies”, Parappanpoil, Thamarassery, Calicut as as raw material

distributor for Baekry only.</td>
                                </tr>
                                <tr>
                                    <td>2012</td>
                                    <td>Started Distribution for General Stores. Also.</td>
                                </tr>
                                <tr>
                                    <td>2013</td>
                                    <td>Expanded line distribution for Wayanad District.</td>
                                </tr>
                                <tr>
                                    <td>2014</td>
                                    <td>Set up of “RB Trade India” as a sister concern and began operations</td>
                                </tr>
                                <tr>
                                    <td>2015</td>
                                    <td>Attached all range of products for being an ultimate solution for Hotel, Bakery and Catering Units (HBC)</td>
                                </tr>
                                <tr>
                                    <td>2016</td>
                                    <td>Expanded to Kannur District as “Flavor Mart” as sister concern</td>
                                </tr>
                                <tr>
                                    <td>2017</td>
                                    <td>Opening soon a branch of “Flavor mart” at Kalpetta- Wayanad</td>
                                </tr>
                            </tbody>
                        </table>
                        <br />
                        <h4>Awards and Achievements</h4>
                        <table class="mile_stones table table-bordered">
                            <tbody>
                                <tr>
                                    <td>2009</td>
                                    <td>Certificate of Appreciation for outstanding performance from Buge India.</td>
                                </tr>
                                <tr>
                                    <td>2010</td>
                                    <td>Certificate of Dealership from Symega Flavour formerly known as Aromco India Pvt Ltd</td>
                                </tr>
                                <tr>
                                    <td>2011</td>
                                    <td>Certificate of Appreciation for outstanding performance from Bunge India.</td>
                                </tr>
                                <tr>
                                    <td>2012</td>
                                    <td>Special Award and Trophy for achieving highest growth from Bunge India</td>
                                </tr>
                                <tr>
                                    <td>2013</td>
                                    <td>First Highest Sales Award from 3f Industries formerly known as Foods Fats and Fertilisers Ltd</td>
                                </tr>
                                <tr>
                                    <td>2014</td>
                                    <td>Special complement from Gold Winner for contributions</td>
                                </tr>
                                <tr>
                                    <td>2015</td>
                                    <td>Award from Curst and Crumb for outstanding Performance</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>    
        </div>
    </div>
<?php require("footer1.php"); ?>
<?php require("footer2.php"); ?>