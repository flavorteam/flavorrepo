<footer>
        <div class="container">
            <div class="col-lg-4 col-md-4 col-sm-4">
                <div class="ft_log"><a href="index.php"><img src="images/ft_log.png" alt=""></a></div>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-3">
                <div class="adrs">
                    <p>RB Trade India,<br />Rolex Arcade,<br />Parappanpoil PO,<br />Thamarassery VIA,<br />Calicut, Kerala-673573
                        <br /> Email : <a href="mailto:rbtradeindia@gmail.com">rbtradeindia@gmail.com</a></p>
                </div>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-3">
                <div class="foot_nav">
                    <ul>
                        <li><a href="index.php">Home</a></li>
                        <li><a href="about_us.php">Our Company</a></li>
                        <li><a href="products.php">Products</a></li>
                        <li><a href="services.php">Services</a></li>
                        <li><a href="gallery.php">Gallery</a></li>
                        <!--<li><a href="facilities.php">OUR FACILITES</a></li>
                        <li><a href="subdealers.php">OUR SUBDEALERS</a></li>
                        <li><a href="mode_of.php">MODE OF OPERATION</a></li>-->
                        <li><a href="contact_us.php">Contact Us</a></li>
                    </ul>
                </div>
            </div>
            <div class="col-lg-2 col-md-2 col-sm-2">
                <div class="track_nav_ft">
                    <ul>
                        <li><a href="terms_and_conditions">Terms and Conditions</a></li>
                        <li><a href="recipie.php">Recipes</a></li>
                    </ul>
                </div>
                <div class="social">
                    <a href="#"><span><i class="fa fa-facebook-f"></i></span></a>
                    <a href="#"><span><i class="fa fa-twitter"></i></span></a>
                </div>
            </div>
        </div>
    </footer>
    <div class="foot_bar">
        <div class="container">
            <p class="copy">Copyright &copy; 2017 Flavor Mart. All Rights Reserved</p>
            <p class="powr">Powerd by<a href="http://www.bodhiinfo.com" target="_blank"> <img src="images/bodhi_logo.png"></a></p>
        </div>
    </div>
<!-- Pop ups -->
<!-- Login and Register -->
<div id="user_modal" class="modal fade bd_modal" role="dialog">
    <div class="modal-dialog modal-sm">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <div class="user_controls">
                    <a href="#login" class="active">Login</a>
                    <a href="#register">Register</a>
                </div>
            </div>
            <div class="modal-body">
                <div class="user_wrap">
                    <div class="user_inner">
                        <div class="user_block active" id="login">
                            <form action="login.php" method="post">
                                <div class="form_block">
                                    <label>User name</label>
                                    <input type="text" name="userName" placeholder="Enter your user name" required="">
                                </div>
                                <div class="form_block">
                                    <label>Password</label>
                                    <input type="password" name="password" placeholder="********" required="">
                                </div>
                                <div class="form_block">
                                    <button type="submit">Sign in</button>
                                </div>
                            </form>
                        </div>
                        <div class="user_block" id="register">
                            <form action="register.php?op=new" method="post">
                                <div class="form_block">
                                    <label>Name</label>
                                    <input type="text" name="name" placeholder="Enter your user name" required="">
                                </div>
                                <div class="form_block">
                                    <label>Email</label>
                                    <input type="email" name="email" placeholder="Enter your user name" required="">
                                </div>
                                 <div class="form_block">
                                    <label>Phone</label>
                                    <input type="text" name="phone" placeholder="Enter your phone no" required="">
                                </div>
                                <div class="form_block">
                                    <label>Address</label>
                                    <textarea name="address" placeholder="Enter your address"></textarea>
                                </div>
                                <div class="form_block">
                                    <label>User Name</label>
                                    <input type="text" name="userName" placeholder="Enter your user name" required="">
                                </div>
                                <div class="form_block">
                                    <label>Password</label>
                                    <input type="password" name="password1" placeholder="********" required="">
                                </div>
                                <div class="form_block">
                                    <label>Confirm password</label>
                                    <input type="password" name="password2" placeholder="********" required="">
                                </div>
                                <div class="form_block">
                                    <button type="submit">Register</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>


<!-- Cart form -->
<div id="checkout_form" class="modal fade bd_modal" role="dialog">
    <div class="modal-dialog modal-md">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Checkout</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                <?php 
                $customerId = $_SESSION['loginId'];
                //echo $customerId;die;
                
                $query = "SELECT ".TABLE_SHIPPING_DETAILS.".ID,
                				 ".TABLE_SHIPPING_DETAILS.".houseName,
                				 ".TABLE_SHIPPING_DETAILS.".houseNo,
                				 ".TABLE_SHIPPING_DETAILS.".street,
                				 ".TABLE_SHIPPING_DETAILS.".city,
                				 ".TABLE_SHIPPING_DETAILS.".district,
                				 ".TABLE_SHIPPING_DETAILS.".state,
                				 ".TABLE_SHIPPING_DETAILS.".country,
                				 ".TABLE_CUSTOMER.".cusName
                			FROM ".TABLE_SHIPPING_DETAILS.",".TABLE_CUSTOMER."
                		   WHERE ".TABLE_SHIPPING_DETAILS.".customerId = $customerId
                		   AND ".TABLE_CUSTOMER.".ID =".TABLE_SHIPPING_DETAILS.".customerId";
                //echo $query;die;
                $qryall = $db->query($query);
                $number1		=	mysql_num_rows($qryall);
                $i = 1;
                if($number1 >0) 
                {
                	while($qryRow = mysql_fetch_array($qryall)) 
                	{
                		$shippingId = @$qryRow['ID'];
						?>
						<div class="col-lg-6 col-md-6 col-sm-6 shipping_address">
						<form method="post" action="shipping.php?op=new&id=<?= $shippingId ?>"> 
						<p><?php echo $qryRow['houseName']; ?><br /><?php echo $qryRow['houseNo']; ?><br /><?php echo $qryRow['street']; ?><br /><?php echo $qryRow['city']; ?><br /><?php echo $qryRow['district']; ?><br /><?php echo $qryRow['state']; ?><br /><?php echo $qryRow['country']; ?></p>
						
							<input type="hidden" name="shippingId" value="<?php echo $qryRow['ID']; ?>">
							<button type="submit">Deliver to this address</button>
							<a href="shipping.php?op=delete&id=<?= $shippingId ?>"><i class="fa fa-times"></i>Delete</a>
						</form>
						</div>
					<?php	
					if ($i == 2) {
						echo "</div><div class='row'>";
						$i = 0;
					}
					$i++;
					}//end of while
                	
                } 
                ?>
				</div>
                	<form action="shipping.php?op=new" method="post">
					<br />
                	<div class="row">
                		<div class="col-lg-6 col-md-6 col-sm-6">
		                    <div class="form_block">
		                        <label>House Name</label>
		                        <input type="text" name="hname" value="">
		                    </div>
		                    <div class="form_block">
		                        <label>House No</label>
		                        <input type="text" name="hno" value="">
		                    </div>
		                    <div class="form_block">
		                        <label>Street</label>
		                        <input type="text" name="street" value="">
		                    </div>
		                    <div class="form_block">
		                        <label>City</label>
		                        <input type="text" name="city" value="">
		                    </div>
                		</div>
                		<div class="col-lg-6 col-md-6 col-sm-6">
                			
		                   	<div class="form_block">
		                        <label>District</label>
		                        <input type="text" name="district" value="">
		                    </div>
		                    <div class="form_block">
		                        <label>State</label>
		                        <input type="text" name="state" value="">
		                    </div>
		                    <div class="form_block">
		                        <label>Country</label>
		                        <input type="text" name="country" value="">
		                    </div>
                		</div>
                	</div>
                    <div class="form_block">
                        <button type="submit">Proceed</button>
                    </div>
                </form>
            </div>
        </div>

    </div>
</div>
