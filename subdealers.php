<?php require("header.php"); ?>
    <div class="cat_banr_part contact_pg">
        <div class="container">
            <div class="row">
                <div class="col-lg-3 col-md-3">
                    <h4>CATEGORIES<span class="cat_trigger"><i class="fa fa-plus"></i></span></h4>

                    <?php

                    $selcatQuery = "SELECT * FROM ".TABLE_CATEGORIES."";
                    $selectcatAll= $db->query($selcatQuery);

                    ?><ul class="categories"><?php
                        while($catRows = mysql_fetch_array($selectcatAll))
                        {
                            ?>
                            <li><a href="products.php?cat=<?php echo $catRows['ID'] ?>"><?php echo $catRows['categoryName'] ?></a></li>



                        <?php } ?>
                    </ul>
                </div>
                <div class="col-lg-9 col-md-9">
                    <div class="about_pg_part">
                        <h2>Our Subdealers</h2>
                    </div>
                    <div class="sub_dealers_wrap">
                        <div class="row">
                            <div class="col-lg-4 col-md-4 col-sm-4">
                                <div class="sub_dealer_img">
                                    <img src="images/we_deal_1.png" alt="" />
                                </div>
                                <div class="sub_dealer_det">
                                    <h4>KOG-KTV FOOD PRODUCTS</h4>
                                    <p>PPR Complex, Nr.Edakkad Village Office<br />
                                        Kizhunna PO, Thottada,<br />
                                        Kannur - 670007, Kerala, India<br />
                                        Mob : +91 9349303767<br />
                                        Email : flavormart@gmail.com</p>
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-4 col-sm-4">
                                <div class="sub_dealer_img">
                                    <img src="images/we_deal_2.png" alt="" />
                                </div>
                                <div class="sub_dealer_det">
                                    <h4>ADANI WILMAR</h4>
                                    <p>PPR Complex, Nr.Edakkad Village Office<br />
                                        Kizhunna PO, Thottada,<br />
                                        Kannur - 670007, Kerala, India<br />
                                        Mob : +91 9349303767<br />
                                        Email : flavormart@gmail.com</p>
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-4 col-sm-4">
                                <div class="sub_dealer_img">
                                    <img src="images/we_deal_3.png" alt="" />
                                </div>
                                <div class="sub_dealer_det">
                                    <h4>MASTERLINE</h4>
                                    <p>PPR Complex, Nr.Edakkad Village Office<br />
                                        Kizhunna PO, Thottada,<br />
                                        Kannur - 670007, Kerala, India<br />
                                        Mob : +91 9349303767<br />
                                        Email : flavormart@gmail.com</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php require("footer1.php"); ?>
<?php require("footer2.php"); ?>