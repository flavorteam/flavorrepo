<?php
require("flavoradmin/config/config.inc.php"); 
require("flavoradmin/config/Database.class.php");
require("flavoradmin/config/Application.class.php");

$optype = (strtolower(empty($_POST['op']))) ? ((strtolower(empty($_GET['op']))) ? $_REQUEST['op'] : $_GET['op']) : $_POST['op'];

switch ($optype) 
{
    // NEW SECTION
    case 'new':

        if (!$_REQUEST['id']) 
        {
            $_SESSION['msg'] = $App->sessionMsgCreate("error", "Error, Invalid Details!<br />Please try again.");
            header("location:index.php");
        } 
        else 
        {
            $db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE);
            $db->connect();
            $success = 0;
            			
            $productId		= $App->convert($_REQUEST['id']);                 			
			$customerId = $_SESSION['loginId'];
            $existId = $db->existValuesId(TABLE_CART, "productId='$productId' and customerId='$customerId'");
            //echo $existId;
            if ($existId > 0) 
            {
               	$qry =mysql_query("SELECT quantity FROM ".TABLE_CART." WHERE ID='{$existId}'");
               	//echo $qry;die;
               	$qryResult = mysql_fetch_array($qry);
               	$lastValue = $qryResult['quantity'];
                $data['quantity']		= $lastValue+1;
                
                
                $success = $db->query_update(TABLE_CART, $data,"ID=$existId");
				//echo $success;die;
                /*if ($success) 
                {
                    //$_SESSION['msg'] = " Details Added Successfully";
                    $_SESSION['msg'] = $App->sessionMsgCreate('success', "Details added successfully");                
                } 
                else 
                {
                    $_SESSION['msg'] = $App->sessionMsgCreate('error', "Failed to add details. Please try again.");                   
                }*/
                 header("location:index.php");
            }
            else 
            {
            	$prizeQry	=	mysql_query("SELECT flavormartRate FROM ".TABLE_PRODUCT_PURCHASE."
            								 WHERE ID=$productId");

            	$prizeRow	=	mysql_fetch_array($prizeQry);
            	$prize		=	$prizeRow['flavormartRate'];
               	$data['productId']		=	$productId;
                $data['customerId']		=	$customerId;
              	$data['quantity']		= 	1;
              	$data['amount']			=	$prize;
                $success = $db->query_insert(TABLE_CART, $data);
                header("location:index.php");  
            }
            $db->close();
        }
        break;
	// Update SECTION
    case 'edit':
    	$editId		=	$_REQUEST['id'];
    	$db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE);
        $db->connect();
        $success = 0;
    	
    	$data['quantity']	=	$App->convert($_REQUEST['quantity']);
    	$success 			= 	$db->query_update(TABLE_CART, $data," ID=$editId ");
    	$db->close();
		header("location:cart.php");  
    break;
    // DELETE SECTION
    case 'delete':
        $deleteId = $_REQUEST['id'];
        $success = 0;

        $db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE);
        $db->connect();
  
			$success1= @mysql_query("DELETE FROM `".TABLE_CART."` WHERE ID='{$deleteId}'");
			
        $db->close();
        header("location:cart.php");
    break;
        
  //for displaying the comment while unregistered user doing cart.        
    case 'refresh': 
   
     		$_SESSION['msg'] = $App->sessionMsgCreate('error', "Sorry..! Please Login First.!");
      		header("location:index.php");
     break;    
}
?>